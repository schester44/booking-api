<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * App
 *
 * @property AppControl $appcontrol
 * @property AppSdk     $appsdk
 * @property AppUi      $appui
 * @property CI_Loader  $load
 * @property IW_Input   $input
 * @property IW_Output  $output
 */
class App extends CI_Controller
{
	public function index()
	{
		$this->load->library( 'AppSdk' );

		switch( basename( parse_url( $this->input->server( 'REQUEST_URI' ), PHP_URL_PATH ) ) ) {
			case 'ui.php':
				$this->ui(
					$this->input->get( 'session' ),
					$this->input->get( 'sessionId' ),
					$this->input->get( 'user' )
				);
				break;

			case 'control.php':
				$this->control( $this->appsdk->getControlCommand() );
				break;

			default:
				redirect( base_url( 'app/' ) );
		}
	}

	/**
	 * @param string $session
	 * @param string $session_id
	 * @param string $user
	 */
	private function ui( $session, $session_id, $user )
	{
		$this->load->library( 'AppUi' );

		if( ( ENVIRONMENT === 'production' ) && !$this->appui->validateSession( $session, $session_id, $user ) ) {
			show_error( 'Your session may have expired. Try logging in again.', 403, 'Unauthorized Access' );
		}

		redirect( base_url( 'dashboard/' ) );
	}

	/**
	 * @param string $command
	 */
	private function control( $command )
	{
		if( ( ENVIRONMENT !== 'development' ) && !$this->appsdk->authenticate() ) {
			show_error( 'Invalid token.', 403, 'Unauthorized Access' );
		}

		$this->load->library( 'AppControl' );

		if( !is_callable( [ $this->appcontrol, $command ] ) ) {
			log_message( 'error',
				__METHOD__ . ": '{$command}' is not a valid command; referrer: {$this->input->server( 'HTTP_REFERER' )}"
			);

			$this->output->set_status_header( 400 );
			return;
		}

		$this->appcontrol->$command();
	}
}
