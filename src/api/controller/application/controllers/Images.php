<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Images
 *
 * @property CI_Config    $config
 * @property CI_Loader    $load
 * @property Images_model $images
 * @property IW_Input     $input
 * @property IW_Output    $output
 */
class Images extends CI_Controller
{
	/**
	 * constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model( 'Image_model', 'images' );
	}

	/**
	 * @param int    $id
	 * @param string $format
	 */
	public function index( $id = null, $format = null )
	{
		$id = (int)$id ?: null;

		if( $id === null ) {
			log_message( 'error',
				__METHOD__ . ": No ID given; referrer: {$this->input->server( 'HTTP_REFERER' )}"
			);

			$this->output->json( [ 'error' => 'No ID given' ], 400 );
			return;
		}

		$image = $this->images->get( $id );


		if( $image === false ) {
			$this->output->json( [ 'error' => $this->images->getLastError()['message'] ], 500 );
			return;
		} elseif( $image === null ) {
			$this->output->set_status_header( 404 );
			return;
		}


		switch( $format ) {
			case 'thumbnail':
				if( !file_exists( $image['thumbnail_filepath'] ) ) {
					$thumbnail_created = $this->images->createThumbnail( $image['filepath'] );

					if( !$thumbnail_created ) {
						$error = $this->images->getLastError();
						log_message( 'error',
							__METHOD__ . ": Failed to create thumbnail: {$error['message']}"
						);
					}
				}

				$filepath = $image['thumbnail_filepath'];
				break;

			default:
				$filepath = $image['filepath'];
				break;
		}

		if( !file_exists( $filepath ) ) {
			log_message( 'error',
				__METHOD__ . ": '{$filepath}' does not exist"
			);

			$this->output->set_status_header( 404 );
			return;
		}

		header( 'Content-Type: ' . mime_content_type( $filepath ) );
		readfile( $filepath );
	}
}