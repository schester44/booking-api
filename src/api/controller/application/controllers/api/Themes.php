<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Themes extends IW_REST_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model( 'Theme_model', 'themes' );
	}

	public function index( $id = null )
	{
		switch( $this->input->method() ) {
			case 'get':
				$this->get( $id );
				break;
			case 'post':
				$this->create( $this->input->input_stream_json() );
				break;
			case 'delete':
				$this->delete( $id );
				break;
			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET, POST, DELETE, OPTIONS' );
				break;
			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, POST, DELETE, OPTIONS' );
		}
	}

	public function updateDefaultTheme()
	{
		$theme = $this->themes->get( 1 );

		if ( !$theme ) {
			echo 'Theme does not exist';
			return;
		}

		$properties = @file_get_contents( $this->config->item( 'data_path' ) . 'themes/default.json' );

		if ( !$properties ) {
			echo 'Error while reading themes/default.json';
			return;
		}

		$updated = $this->themes->update(1, [ 'properties' => json_decode($properties) ] );

		echo '<pre>';
		var_dump( $updated );
		echo '</pre>';
	}

	public function get( $id )
	{
		$themes = $this->themes->get( $id );

		$this->output->json( $themes );
	}

	public function create( array $data )
	{
		$created = $this->themes->create( $data );

		if ( $created === false ) {
			log_message( "error", __METHOD__ . ": Failed to create a theme." );
			$error = $this->themes->getLastError();

			$this->output->json( [ 'error' => @$error['message'] ?: 'Failed to create the theme' ], 500 );
			return false;	
		}

		$this->output->json( $created );
	}

	public function update( $id )
	{
		$data = $this->input->post();
		
		$updated = $this->themes->update( $id, $data );

		if ( $updated === false ) {
			log_message( "error", __METHOD__ . ": Failed to update theme #{$id}." );
			$error = $this->themes->getLastError();

			$this->output->json( [ 'error' => @$error['message'] ?: 'Failed to update the theme' ], 500 );
			return false;	
		}

		$this->output->set_status_header( 204 );
	}

	public function delete( $id )
	{
		$deleted = $this->themes->delete( $id );

		if ( $deleted === false ) {
			log_message( "error", __METHOD__ . ": Failed to delete theme #{$id}" );
			$error = $this->themes->getLastError();

			$this->output->json( [ 'error' => @$error['message'] ?: 'Failed to delete the theme' ], 500 );
			return false;
		}

		$this->output->set_status_header( 204 );
	}
}