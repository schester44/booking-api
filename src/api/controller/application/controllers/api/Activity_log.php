<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Activity_log
 *
 * @property Activity_log_model $activity_log
 * @property CI_Config          $config
 * @property IW_Input           $input
 * @property IW_Output          $output
 */
class Activity_log extends IW_REST_Controller
{
	/**
	 * Provides a REST-like endpoint.
	 *
	 * @param int $id
	 */
	public function index( $id = null )
	{
		switch( $this->input->method() ) {
			case 'get':
				$this->get( $id );
				break;

			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET' );
				break;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, OPTIONS' );
		}
	}

	/**
	 * Return TRUE/FALSE to indicate whether or not there are unread activities. Can
	 * also be used to mark all activities as read.
	 */
	public function status()
	{
		switch( $this->input->method() ) {
			case 'get':
				$this->output->set_output_json( [ 'status' => $this->activity_log->hasUnread() ] );
				return;

			case 'post':
				$updated = $this->activity_log->markAllRead();

				if( $updated === false ) {
					$this->output->set_output_json( [ 'error' => $this->activity_log->getLastError()['message'] ], 500 );
					return;
				}

				$this->output->set_status_header( 204 ); // No Content
				return;

			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET, POST' );
				return;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, POST, OPTIONS' );
		}
	}

	/**
	 * Creates a spreadsheet and forces a download.
	 */
	public function export()
	{
		if( $this->input->method() === 'options' ) {
			$this->output->set_header( 'Access-Control-Allow-Methods: GET' );
			return;
		} elseif( $this->input->method() !== 'get' ) {
			$this->output->set_status_header( 405 );
			$this->output->set_header( 'Allow: GET, OPTIONS' );
			return;
		}

		$events = $this->activity_log->get();

		if( $events === false ) {
			$this->output->set_status_header( 500 );
			return;
		}

		$spreadsheet = new PHPExcel();
		$sheet       = $spreadsheet->setActiveSheetIndex( 0 );

		// Create a header row.
		$sheet->setCellValue( 'A1', 'Time' );
		$sheet->setCellValue( 'B1', 'User' );
		$sheet->setCellValue( 'C1', 'Message' );

		// Style the header row.
		$sheet->getStyle( '1' )->getFont()->setBold( true );

		foreach( array_values( $events ) as $i => $event ) {
			$row = $i + 2;

			$sheet->setCellValue( "A{$row}", date( 'Y-m-d h:i:s A', $event['timestamp'] ) );
			$sheet->setCellValue( "B{$row}", $event['user'] );
			$sheet->setCellValue( "C{$row}", $event['message'] );
		}

		// Auto-resize the columns.
		foreach( $sheet->getColumnIterator( 'A', $sheet->getHighestDataColumn() ) as $column ) {
			$sheet->getColumnDimension( $column->getColumnIndex() )->setAutoSize( true );
		}

		$tmp_filepath = $this->config->item( 'cache_path' ) . 'activity_log.xlsx';

		$writer = PHPExcel_IOFactory::createWriter( $spreadsheet, 'Excel2007' );
		$writer->save( $tmp_filepath );

		$this->load->helper( 'download' );
		force_download( $tmp_filepath, null );
	}

	/**
	 * @param int $id
	 */
	private function get( $id )
	{
		$response = $this->activity_log->get( $id );

		if( $response === false ) {
			$this->output->set_output_json( [ 'error' => $this->activity_log->getLastError()['message'] ], 500 );
			return;
		} elseif( $response === null ) {
			$this->output->set_status_header( 404 );
			return;
		}

		// If we're sending back all events, drop the array indexes.
		if( $id === null ) {
			$response = array_values( $response );

			// Also, limit the number of events to a maximum of 100.
			$response = array_slice( $response, 0, 100 );
		}

		$this->output->set_output_json( $response );
	}
}
