<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Services extends IW_REST_Controller
{
	public function index( $id = null )
	{
		switch( $this->input->method() ) {
			case 'get':
				$this->get( $id );
				break;

			case 'post':
				$this->update( $id, $this->input->post() );
				break;
			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET, POST, OPTIONS' );
				break;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, POST, OPTIONS' );
		}
	}

	public function get( $id )
	{
		$this->load->model( 'service_model', 'services' );

		$services = $this->services->get( $id );

		if ( $services === false ) {
			log_error( "error", 
				__METHOD__ . "DB returned 0 services."
			);

			$this->output->set_status_header(404);
			return;
		}

		$this->output->json( $services );
	}

	public function update( $id, array $data )
	{

		$this->load->model( 'service_model', 'services' );

		$service = $this->services->get( $id );

		if ( !$service ) {
			log_message( "debug", 
				__METHOD__ . ": Failed to update a service with ID #{$id}. Service does not exist."
			);

			$this->output->json( [ "error" => "A service with that ID does not exist."], 400 );
			return;
		}
		
		$updated = $this->services->update( $id, $data );

		if ( $updated === false ) {
			$error = $this->services->getLastError();
			log_message( "error",
				__METHOD__ . ": Failed to update a service #{$id}"
			);

			$this->output->json( [ "error" => @$error['message'] ?: 'Failed to update the service.' ], 500 );
			return;
		}

		$this->output->json( 204 );
	}
}