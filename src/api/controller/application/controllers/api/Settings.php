<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Settings extends IW_REST_Controller
{

	public function index()
	{
		$this->load->model( "Settings_model", "settings" );

		switch( $this->input->method() ) {
			case 'get':
				$this->get();
				break;
			case 'put':
				$this->update( $this->input->input_stream_json() );
				break;
			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET, PUT' );
				break;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, PUT, OPTIONS' );
		}
	}

	public function get()
	{
		$settings = $this->settings->get();

		$this->output->json( $settings );
	}


	public function update( $data )
	{
		$updated = $this->settings->batchUpdate( $data );

		if ( !$updated ) {
			$error = $this->settings->getLastError();
			$this->output->json( [ 'error' => @$error['message'] ?: 'Failed to update the settings.' ], 500 );
			return;
		}

		$this->output->set_status_header( 204 );
	}
}