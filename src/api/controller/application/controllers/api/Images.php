<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Images
 *
 * @property Activity_log_model $activity_log
 * @property CI_Config          $config
 * @property CI_Loader          $load
 * @property Images_model       $images
 * @property IqW_Input           $input
 * @property IW_Output          $output
 */
class Images extends IW_REST_Controller
{
	/**
	 * constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model( 'Image_model', 'images' );
	}

	/**
	 * Provides a REST-like endpoint.
	 *
	 * @param int $id
	 */
	public function index( $id = null )
	{
		$id = (int)$id ?: null;

		switch( $this->input->method() ) {
			case 'get':
				$this->get( $id );
				break;

			case 'post':
				( $id === null )
					? $this->create( $this->input->post() )
					: $this->update( $id, $this->input->post() );
				break;

			case 'delete':
				$this->delete( $id );
				break;

			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET, POST, DELETE' );
				break;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, POST, DELETE, OPTIONS' );
		}
	}

	/**
	 * @param string $category
	 */
	public function category( $category = null )
	{
		if ( $this->input->method() === 'options' ) {
			$this->output->set_header( 'Access-Control-Allow-Methods: GET' );
			return;
		} elseif( $this->input->method() !== 'get' ) {
			$this->output->set_status_header( 405 );
			$this->output->set_header( 'Allow: GET, OPTIONS' );
			return;
		}

		if ( $category === null ) {
			log_message( 'error',
				__METHOD__ . ": No category given; referrer: {$this->input->server( 'HTTP_REFERER' )}"
			);

			$this->output->json( [ 'error' => 'No category given.' ], 400 );
			return;
		}

		$response = $this->images->getByCategory( $category );

		if ( $response === false ) {
			$this->output->json( [ 'error' => $this->images->getLastError()['message'] ], 500 );
			return;
		}

		$this->output->json( $this->images->transformOutput( $response, true ) );
	}

	/**
	 * @param int $id
	 */
	private function get( $id )
	{
		$response = $this->images->get( $id );

		if( $response === false ) {
			$this->output->json( [ 'error' => $this->images->getLastError()['message'] ], 500 );
			return;
		} elseif( $response === null ) {
			$this->output->set_status_header( 404 );
			return;
		}

		$this->output->json( $this->images->transformOutput( $response, ( $id === null ) ) );
	}

	/**
	 * @param array $input
	 */
	private function create( array $input )
	{
		$image = $this->images->create( $this->transformInput( $input ) );

		if( $image === false ) {
			$error = $this->images->getLastError();
			log_message( 'error',
				__METHOD__ . ": Failed to upload image: {$error['message']} ({$error['code']})"
			);

			$this->output->json( [ 'error' => $error['message'] ], 500 );
			return;
		}

		$this->activity_log->write( Activity_log_model::ACTION_CREATE,
			"Image \"{$image['name']}\" ({$image['id']}) created."
		);

		$this->output->json( $this->images->transformOutput( $image ), 201 );
	}

	/**
	 * @param string $id
	 * @param array  $input
	 */
	private function update( $id, array $input )
	{
		if( $id === null ) {
			log_message( 'error',
				__METHOD__ . ": No ID given; referrer: {$this->input->server( 'HTTP_REFERER' )}"
			);

			$this->output->json( [ 'error' => 'No ID given' ], 400 );
			return;
		}

		if( !$this->images->exists( $id ) ) {
			$this->output->set_status_header( 404 );
			return;
		}

		$image = $this->images->update( $id, $this->transformInput( $input ) );

		if( $image === false ) {
			$this->output->json( [ 'error' => $this->images->getLastError()['message'] ], 500 );
			return;
		}

		$this->activity_log->write( Activity_log_model::ACTION_UPDATE,
			"Image \"{$image['name']}\" ({$image['id']}) updated."
		);

		$this->output->set_status_header( 204 ); // No Content
	}

	/**
	 * @param int $id
	 */
	private function delete( $id )
	{
		if( $id === null ) {
			log_message( 'error',
				__METHOD__ . ": No ID given; referrer: {$this->input->server( 'HTTP_REFERER' )}"
			);

			$this->output->json( [ 'error' => 'No ID given' ], 400 );
			return;
		}

		$image = $this->images->delete( $id );

		if( $image === false ) {
			$error = $this->images->getLastError();
			log_message( 'error',
				__METHOD__ . ": Failed to delete image #{$id}: [{$error['code']}] {$error['message']}"
			);

			switch( $error['code'] ) {
				case '23000/1451':
					$error_message = 'This image is being used and can not be deleted.';
					break;

				default:
					$error_message = $error['message'];
			}

			$this->output->json( [ 'error' => $error_message ], 500 );
			return;
		}

		$this->activity_log->write( Activity_log_model::ACTION_DELETE,
			"Image \"{$image['name']}\" ({$image['id']}) deleted."
		);

		$this->output->set_status_header( 204 ); // No Content
	}

	/**
	 * @param array $input
	 *
	 * @return array
	 */
	private function transformInput( array $input )
	{
		return array_intersect_key( $input, array_flip( [
			'name', 'category'
		] ) );
	}
}