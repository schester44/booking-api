<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Rooms extends IW_REST_Controller
{
	public function index( $id = null )
	{
		$this->load->model( 'room_model', 'rooms' );

		switch( $this->input->method() ) {
			case 'get':
				$this->get( $id );
				break;

			case 'post':
				$this->create( $this->input->input_stream_json() );
				break;

			case 'put':
				$this->update( $this->input->input_stream_json() );
				break;

			case 'delete':
				$this->delete( $id );
				break;

			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET, POST, PUT, DELETE' );
				break;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, POST, PUT, DELETE, OPTIONS' );
		}

	}

	public function get( $id )
	{
		$rooms = $this->rooms->get( $id );

		if ( $rooms === false ) {
			$this->output->json( [], 404 );
			return;
		}

		$this->output->json( $rooms );
	}

	public function create( $inputData )
	{
		$created = $this->rooms->create( $inputData );

		if ( $created === false ) {
			$error = $this->rooms->getlastError();
			log_message( "error", __METHOD__ . ": Failed to create a room" );
			
			$this->output->json( [ 'error' => @$error['message'] ?: 'Failed to create the room' ], 500 );
			return;
		}

		$this->output->json( $created, 201 );
	}

	public function update( array $inputData )
	{
		$updated = $this->rooms->update( $inputData['id'], $inputData );

		if ( $updated === false ) {
			log_message( "error", __METHOD__ . ": Failed to update a room" );
			
			$this->output->json( [ 'error' => 'Failed to update the room'], 500 );
			return;
		}

		$this->output->json( $updated, 204 );
	}

	public function delete( $id )
	{
		$deleted = $this->rooms->delete( $id );

		if ( !$deleted ) {
			log_message( "error",
				__METHOD__ . ": Failed to delete room ({$id})"
			);
			
			$this->output->json( [ 'error' => 'Failed to delete the room'], 500 );
			return;
		}

		$this->output->set_status_header( 204 );
	}
}
