<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Events extends IW_REST_Controller
{
	public function index( $id = null )
	{
		switch( $this->input->method() ) {
			case 'get':
				$this->get( $id );
				break;
			case 'post':
				$this->create( $id, $this->input->input_stream_json() );
				break;
			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET, POST' );
				break;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, POST, OPTIONS' );
		}
	}

	public function get( $id )
	{	
		$this->load->model( 'event_model', 'events' );

		$events = $this->events->get( $id );

		if ( $events === false ) {
			$error = $this->events->getLastError();
			$this->output->json( [ 'error' => @$error['message'] ?: 'No events were found.' ], 500 );
			return;
		}

		$this->output->json( $events );
	}

	public function create( $id, $data )
	{
		$this->load->model( 'event_model', 'events' );

		$data['room_id'] = isset( $data['room_id'] ) ? $data['room_id'] : $id;

		$created = $this->events->create( $data );

		if ( $created === false ) {
			$error = $this->events->getLastError();

			log_message( "error",
				__METHOD__ . ": Failed to create an event for room #{$id}.  ({$error['message']})"
			);

			$this->output->json( [ 'error' => @$error['message'] ?: "Failed to create the event." ], 500 );
			return;		
		}

		$this->output->set_status_header( 201 );
	}

	/**
	 * QueueManager endpoint for creating jobs.
	 * @param  [string] $service - name of the event source service
	 * @param  [array] $config - config for the serice
	 * @param  [array] $event - the actual event to create
	 * @return [bool]
	 */
	public function delayed_create( $service, $config, $event )
	{
		$this->load->library( "services/{$service}", null, $service );
		$this->$service->init( (array) $config );

		$created = $this->$service->createEvent( (array) $event );

		if ( $created === false ) {
			$error = $this->$service->getLastError();

			log_message( "error",
				__METHOD__ . "failed to create an event for the {$service} service. : ({$error})"
			);

			// filter out the event that failed
			$cache = $this->purgedCache( $config['room_id'], (array) $event );

			$this->load->driver( 'cache', [ 'adapter' => 'file' ] );
			$this->cache->save( $this->$service->cacheConfig['file'], $cache, $this->$service->cacheConfig['duration'] );
		}

		return true;
	}

	private function purgeCache( $id, array $event )
	{
		$cache = $this->cache->get( "events/{$id}" );

		if ( !$cache || empty( $cache ) ) {
			return [ $event ];
		}

		// filter out the event that failed
		return array_filter( $cache, function ( $ev ) use ( $event ) {
			return ( $ev['title'] !== $event['title'] && $ev['start'] !== $event['start'] );
		});
	}
}
