<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Devices
 *
 * @property CI_Input      $input
 * @property Devices_model $devices
 */
class Devices extends IW_REST_Controller
{
	/**
	 * constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model( 'Device_model', 'devices' );
	}

	/**
	 * Provides a REST-like endpoint.
	 */
	public function index()
	{
		switch( $this->input->method() ) {
			case 'get':
				$this->get();
				break;

			case 'options':
				$this->output->set_header( 'Access-Control-Allow-Methods: GET' );
				break;

			default:
				$this->output->set_status_header( 405 );
				$this->output->set_header( 'Allow: GET, OPTIONS' );
		}
	}

	private function get()
	{
		$response = $this->devices->get();

		if( $response === false ) {
			$this->output->json( [ 'error' => $this->devices->getLastError()['message'] ], 500 );
			return;
		} elseif( $response === null ) {
			$this->output->set_status_header( 404 );
			return;
		}

		$this->output->json( $this->transformOutput( $response, true ) );
	}

	/**
	 * @param array $output
	 * @param bool  $multiple Set to TRUE if the response contains multiple
	 *                        departments, FALSE if it's a single department.
	 *
	 * @return array
	 */
	private function transformOutput( array $output, $multiple = false )
	{
		if( $multiple ) {
			$transformed = [];
			foreach( $output as $o ) {
				$transformed[] = $this->transformOutput( $o );
			}
		} else {
			log_message( 'debug',
				__METHOD__ . ': Before: ' . var_export( $output, true )
			);

			$transformed = array_intersect_key( $output, array_flip( [
				'id', 'name'
			] ) );

			log_message( 'debug',
				__METHOD__ . ': After: ' . var_export( $transformed, true )
			);
		}

		return $transformed;
	}
}