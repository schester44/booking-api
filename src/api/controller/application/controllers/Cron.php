<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Cron extends CI_Controller
{
	/**
	* Deletes then re-creates the event's folder that holds all event cache
	* Runs at 12 midnight, effectively clearing any cached events.
	*/
	public function clean()
	{
		$path = $this->config->item( 'cache_path') . "events";

		if ( !is_dir( $path ) ) {
			create_dir( $path );
			return true;
		};

		$this->load->helper( "directory" );

		remove_dir( $path );
		create_dir( $path );
	}

	public function process_queue()
	{
		$this->load->library( "QueueManager", null, "queue" );
		$this->queue->process();
	}
}