<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

if( !function_exists( 'logged_in' ) ) {

	/**
	 * @return bool
	 */
	function logged_in()
	{
		get_instance()->load->library( 'session' );

		// needed until we figure out how to authenticate kiosk type apps
		return true;
		
		// return isset( $_SESSION['user_id'], $_SESSION['default_group'], $_SESSION['groups'] );
	}
}
