<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

if ( !function_exists( 'serviceNameFromId' ) ) {

	/**
	 * @return bool
	 */
	function serviceNameFromId( $id )
	{
		$CI =& get_instance();
		
		$CI->load->model( 'service_model', 'services' );

		$service = $CI->services->get( $id );

		return @$service['name'] ?: false;
	}
}
