<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

if ( !function_exists( 'numberToTimestamp' ) ) {
	function numberToTimestamp( $hours )
	{

		$minutes = fmod( sprintf( "%.2f", $hours ), 1.0 ) * 60;

		if ( $minutes == 0 ) {
			$minutes = "00";
		}

		$hours = floor( $hours );

		if ( strlen( $hours ) === 1 ) {
			$hours = "0" . $hours;
		}

		$time = date( "m-d-y" ) . " " . $hours . ":" . $minutes;

		$dt = DateTime::createFromFormat( "m-d-y H:i", $time );
		$ts = $dt->getTimestamp();
		
		return $ts;
	}
}

if ( !function_exists( 'timestampToNumber' ) ) {
	function timestampToNumber( $timestamp, $timezone = 'America/New_York' )
	{
		
		$time = new DateTime( date('Y-m-d H:i:s', $timestamp ), new DateTimeZone( 'UTC' ) );
		$time->setTimeZone( new DateTimeZone( $timezone ) );

		$hours = (int) $time->format( 'H' );
		$minutes = (int) $time->format( 'i' );

		// by default, hours will return 0 for midnight (24 hr time). We need to set midnight to its actual value which is 24.
		$hours = $hours === 0 ? 24 : $hours;

		return $hours + ( $minutes / 60 );
	}
}
