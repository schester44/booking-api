<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

if( !function_exists( 'create_dir' ) ) {

	/**
	 * @param string $dirname
	 *
	 * @return bool
	 */
	function create_dir( $dirname )
	{
		// If the directory already exists, consider this a success.
		if( is_dir( $dirname ) ) {
			return true;
		}

		return mkdir( $dirname, DIR_WRITE_MODE, true );
	}
}

if( !function_exists( 'remove_dir' ) ) {

	/**
	 * @param string $dirname
	 *
	 * @return bool
	 */
	function remove_dir( $dirname )
	{
		// If the location doesn't exist, consider this a success.
		if( !file_exists( $dirname ) ) {
			return true;
		}

		// If the location does exist, but it's not a directory, abort.
		if( file_exists( $dirname ) && !is_dir( $dirname ) ) {
			return false;
		}

		$items = scandir( $dirname );

		if( $items === false ) {
			return false;
		}

		// Remove the "dot" directories.
		$items = array_diff( $items, [ '.', '..' ] );

		foreach( $items as $item ) {
			$path     = "{$dirname}/{$item}";
			$function = ( is_dir( $path ) ? 'remove_dir' : 'unlink' );

			$deleted = $function( $path );

			if( !$deleted ) {
				return false;
			}
		}

		return rmdir( $dirname );
	}
}
