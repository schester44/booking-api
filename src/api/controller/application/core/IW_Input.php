<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * IW_Input
 *
 * @property string $raw_input_stream
 */
class IW_Input extends CI_Input
{
	/**
	 * @param string $index
	 * @param bool   $xss_clean
	 *
	 * @return mixed
	 */
	public function input_stream_json( $index = null, $xss_clean = null )
	{
		if ( !is_array( $this->_input_stream ) ) {
			$input_stream        = json_decode( $this->raw_input_stream, true );
			$this->_input_stream = is_array( $input_stream ) ? $input_stream : [];
		}

		return $this->_fetch_from_array( $this->_input_stream, $index, $xss_clean );
	}
}
