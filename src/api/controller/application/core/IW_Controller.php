<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * IW_Controller
 *
 * @property CI_Loader $load
 * @property IW_Output $output
 */
class IW_Controller extends CI_Controller
{
	/**
	 * constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if( ENVIRONMENT === 'production' ) {
			$this->load->helper( 'auth' );

			if( !logged_in() ) {
				$this->output->set_status_header( 403 );
				exit();
			}
		}
	}
}
