<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * IW_Model
 *
 * @property CI_DB_query_builder $db
 */
class IW_Model extends CI_Model
{
	/** @var string */
	protected $table;

	/** @var array */
	protected $error = [
		'code'    => null,
		'message' => null
	];

	/**
	 * @param int $id
	 *
	 * @return array|null|false
	 */
	public function get( $id = null )
	{
		$this->db->select()->from( $this->table );

		if ( $id !== null ) {
			$this->db->where( is_array( $id ) ? $id : array( 'id' => $id ) );
		}

		$result = $this->db->get();

		if( $result === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		$rows = $this->transformResult( $result->result_array() );

		return ( $id === null ) ? array_values( $rows ) : @$rows[$id];
	}

	/**
	 * @param array $data
	 *
	 * @return array|false The created item on success, or FALSE on failure.
	 */
	public function create( array $data )
	{
		$inserted = $this->db->insert( $this->table, $this->transformData( $data ) );

		if ( $inserted === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return $this->get( (int)$this->db->insert_id() );
	}

	/**
	 * @param int   $id
	 * @param array $data
	 *
	 * @return array|false The updated item on success, or FALSE on failure.
	 */
	public function update( $id, array $data )
	{
		$updated = $this->db->update( $this->table, $this->transformData( $data ), [ 'id' => $id ] );

		if ( $updated === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return $this->get( $id );
	}

	/**
	 * @param int $id
	 *
	 * @return array|null|false The deleted item on success, NULL if the ID could
	 *                          not be found, or FALSE on failure.
	 */
	public function delete( $id )
	{
		// If the item doesn't exist in the database, there's nothing for us to do.
		if ( !$this->exists( $id ) ) {
			return null;
		}

		// Get the item so that we can return it after it's been deleted.
		$item = $this->get( $id );

		if( $item === false ) {
			return false;
		}

		$deleted = $this->db->delete( $this->table, [ 'id' => $id ] );

		if ( $deleted === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return $item;
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 */
	public function exists( $id )
	{
		return ( $this->db->from( $this->table )->where( 'id', $id )->count_all_results() > 0 );
	}

	/**
	 * @return array
	 */
	public function getLastError()
	{
		return $this->error;
	}

	/**
	 * @param string     $message
	 * @param int|string $code
	 *
	 * @return IW_Model
	 */
	protected function setError( $message, $code = null )
	{
		$this->error = [
			'code'    => $code,
			'message' => $message
		];

		return $this;
	}

	/**
	 * @param array $data
	 *
	 * @return array
	 */
	protected function transformData( array $data )
	{
		return $data;
	}

	/**
	 * @param array $result
	 *
	 * @return array
	 */
	protected function transformResult( array $result )
	{
		return $result;
	}
}
