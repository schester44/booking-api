<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * IW_Output
 */
class IW_Output extends CI_Output
{
	/**
	 * @param string $output
	 * @param int    $status_code
	 *
	 * @return IW_Output
	 */
	public function set_output( $output, $status_code = null )
	{
		if( $status_code !== null ) {
			$this->set_status_header( $status_code );
		}

		parent::set_output( $output );

		return $this;
	}

	/**
	 * @param mixed $output
	 * @param int   $status_code
	 *
	 * @return IW_Output
	 */
	public function json( $output, $status_code = null )
	{
		$this->set_content_type( 'application/json', 'utf8' );
		$this->set_output( json_encode( $output ), $status_code );

		return $this;
	}
}
