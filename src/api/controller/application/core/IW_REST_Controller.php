<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * IW_REST_Controller
 *
 * @property IW_Output $output
 */
class IW_REST_Controller extends IW_Controller
{
	/**
	 * constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->output->set_header( 'Access-Control-Allow-Origin: *' );
		$this->output->set_header( 'Access-Control-Allow-Headers: *' );
	}
}
