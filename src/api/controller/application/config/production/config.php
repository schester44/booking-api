<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

require_once CCHDAASDK_PATH;
$sdk = new CCHDAASDK();

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
*/
$config['base_url'] = "https://{$_SERVER['HTTP_HOST']}" . parse_url( $sdk->getEnv( 'aaDaemonURL' ), PHP_URL_PATH ) . '/';

/*
|--------------------------------------------------------------------------
| Error Logging Directory Path
|--------------------------------------------------------------------------
*/
$config['log_path'] = $sdk->getEnv( 'cachePath' ) . 'logs/';

/*
|--------------------------------------------------------------------------
| Cache Directory Path
|--------------------------------------------------------------------------
*/
$config['cache_path'] = $sdk->getEnv( 'cachePath' );

/*
|--------------------------------------------------------------------------
| Cookie Related Variables
|--------------------------------------------------------------------------
*/
$config['cookie_path'] = parse_url( $sdk->getEnv( 'aaDaemonURL' ), PHP_URL_PATH ) . '/';
