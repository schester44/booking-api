<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

require_once CCHDAASDK_PATH;
$sdk = new CCHDAASDK();

/*
|--------------------------------------------------------------------------
| CommandCenterHD Credentials
|--------------------------------------------------------------------------
*/
$config['cchd_account']  = $sdk->getAccountID();
$config['cchd_user']     = '$token';
$config['cchd_password'] = $sdk->getCCHDToken()['token'];

/*
|--------------------------------------------------------------------------
| App Meta
|--------------------------------------------------------------------------
*/
$config['app_code'] = $sdk->getAppCode();

/*
|--------------------------------------------------------------------------
| Data Directory Path
|--------------------------------------------------------------------------
*/
$config['data_path']  = $sdk->getEnv( 'dataPath' );
$config['theme_path'] = $config['data_path'] . 'themes/';
$config['images_dir'] = $config['data_path'] . 'uploads/images/';