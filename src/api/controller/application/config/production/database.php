<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

require_once CCHDAASDK_PATH;
$sdk = new CCHDAASDK();

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
*/
$active_group  = 'default';
$query_builder = true;

$db['default'] = array(
	'dsn'          => "mysql:host={$sdk->getEnv( 'sqlEndpoint' )};dbname={$sdk->getEnv( 'sqlDatabase' )}",
	'hostname'     => '',
	'username'     => $sdk->getEnv( 'sqlUser' ),
	'password'     => $sdk->getEnv( 'sqlPassword' ),
	'database'     => '',
	'dbdriver'     => 'pdo',
	'dbprefix'     => '',
	'pconnect'     => false,
	'db_debug'     => false,
	'cache_on'     => false,
	'cachedir'     => '',
	'char_set'     => 'utf8',
	'dbcollat'     => 'utf8_general_ci',
	'swap_pre'     => '',
	'encrypt'      => false,
	'compress'     => false,
	'stricton'     => true,
	'failover'     => array(),
	'save_queries' => true
);
