<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']   = 'App';
$route['404_override']         = '';
$route['translate_uri_dashes'] = false;

// This route works around an issue where CI uses the full path of the URL for
// routing if the uri_protocol config is set to "PATH_INFO", but there is no
// PATH_INFO. This happens when control.php, index.php, or ui.php are requested
// without a trailing "/", or if no file is specified at all.
$route[':num/.*'] = 'app/index';

$route['api/rooms/(:num)'] 			= "api/rooms/index/$1";
$route['api/services/(:num)'] 		= "api/services/index/$1";

// events
$route['api/events/([a-zA-Z]+)'] 	= "api/events/$1";
$route['api/events/(:num)'] 		= "api/events/index/$1";

// themes
$route['api/themes/(:num)'] 		= "api/themes/index/$1";
$route['api/themes/(:num)/(:any)'] 	= "api/themes/$2/$1";

// images
$route['api/images/category/(:any)'] = 'api/images/category/$1';
$route['api/images/(:num)']          = 'api/images/index/$1';

$route['images/(:num)/thumbnail']    = 'images/index/$1/thumbnail';
$route['images/(:num)']              = 'images/index/$1';