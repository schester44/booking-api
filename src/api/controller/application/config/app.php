<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/*
-- --------------------------------------------------------------------------
-- CommandCenterHD Credentials
-- --------------------------------------------------------------------------
*/
$config['cchd_account']  = 'dev';
$config['cchd_user']     = '$pin';
$config['cchd_password'] = '123123';
$config['cchd_api_timeout'] = 30;

/*
-- --------------------------------------------------------------------------
-- App Meta
-- --------------------------------------------------------------------------
*/
$config['app_code'] = 'room-booking';

/*
-- --------------------------------------------------------------------------
-- Data Directory Path
-- --------------------------------------------------------------------------
*/

$config['data_path'] = APPPATH . 'data/';
$config['theme_path'] = $config['data_path'] . 'themes/';
$config['images_dir'] = $config['data_path'] . 'uploads/images/';

$config['convert_path'] = "/usr/bin/convert";