<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Activity_log_model
 */
class Activity_log_model extends IW_Model
{
	// Pre-defined actions
	const ACTION_COPY   = 'copy';
	const ACTION_CREATE = 'create';
	const ACTION_DELETE = 'delete';
	const ACTION_ERROR  = 'error';
	const ACTION_UPDATE = 'update';

	/** @var string */
	protected $table = 'activity_log';

	/**
	 * @param string $action
	 * @param string $message
	 * @param string $user
	 *
	 * @return int|false
	 */
	public function write( $action, $message, $user = null )
	{
		return parent::create( [
			'action'  => $action,
			'message' => $message,
			'user'    => $user ?: @$_SESSION['user_id']
		] );
	}

	/**
	 * @return bool
	 */
	public function hasUnread()
	{
		return ( $this->db->from( $this->table )->where( 'unread', true )->count_all_results() > 0 );
	}

	/**
	 * @return bool
	 */
	public function markAllRead()
	{
		$updated = $this->db->update( $this->table, [ 'unread' => false ] );

		if( $updated === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return true;
	}

	/**
	 * Deletes all entries older than the specified number of days.
	 *
	 * @param int $days
	 *
	 * @return bool
	 */
	public function prune( $days = 30 )
	{
		$datetime = new DateTime();
		$datetime->sub( new DateInterval( "P{$days}D" ) );

		$deleted = $this->db->delete( $this->table, [ 'timestamp <' => $datetime->format( 'Y-m-d' ) ] );

		if( $deleted === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return true;
	}

	/**
	 * @param array $data
	 *
	 * @return array
	 */
	protected function transformData( array $data )
	{
		return array_intersect_key( $data, array_flip( [
			'action', 'user', 'message', 'unread'
		] ) );
	}

	/**
	 * @param array $result
	 *
	 * @return array
	 */
	protected function transformResult( array $result )
	{
		log_message( 'debug',
			__METHOD__ . ': Before: ' . var_export( $result, true )
		);

		$transformed = [];

		foreach( $result as $row ) {
			$row['id']        = (int)$row['id'];
			$row['timestamp'] = strtotime( $row['timestamp'] );
			$row['unread']    = ( $row['unread'] === '1' );

			$transformed[$row['id']] = $row;
		}

		log_message( 'debug',
			__METHOD__ . ': After: ' . var_export( $transformed, true )
		);

		return $transformed;
	}
}
