<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Service_model extends IW_Model
{
	protected $table = 'services';

	public function getFields( $id, $showSensitive = false )
	{
		$where = is_array( $id ) ? $id : [ 'service_id' => $id ];

		$this->db->select( 'id, name, display_name, type, required, value' )
			->from( 'service_fields' )
			->where( $where );

		$result = $this->db->get();

		if ( $result === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );
			return false;
		}

		$rows = $result->result_array();

		foreach ( $rows as $key => $row )
		{
			$rows[$key]['required'] = ($row['required'] === "1");

			// don't send back passwords
			if ( $row['type'] === "password" && !$showSensitive ) {
				$rows[$key]['value'] = null;
			}
		}

		return $rows;
	}

	public function getFieldValues( $id, $showSensitive = false )
	{
		$fields = $this->getFields( $id, $showSensitive );

		$transformed = [];

		if ( $fields ) {
			foreach ( $fields as $key => $field )
			{
				$transformed[$field['name']] = $field['value'];
			}			
		}

		return $transformed;
	}

	public function update( $id, array $data )
	{
		if ( !empty( $_FILES ) ) {
			$files_uploaded = $this->uploadFiles( $id, $_FILES );

			if ( $files_uploaded === false ) {
				return false;
			}
		}
		
		$updated = $this->db->update_batch( 'service_fields', $this->transformData( $data ), 'id' ); 


		if ( $updated === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return $this->get( $id );
	}

	protected function uploadFiles( $id, array $files )
	{		
		foreach ( $files as $field_name => $file )
		{
			$service = $this->getFields( [ 'service_id' => $id, 'name' => $field_name ]);

			if ( !$service || empty( $service ) ) {
				continue;
			}

			$received = $this->receiveFile( $field_name );

			if ( !$received ) {
				return false;
			}
		}

		return true;
	}

	private function receiveFile( $field_name )
	{
		$config = [
			'allowed_types'    => '*',
			'file_ext_tolower' => true,
			'overwrite'        => true,
			'upload_path'      => $this->config->item( 'data_path' )
		];

		if ( strtolower( $field_name ) === "pk_file" ) {
			$config['file_name'] = 'gcal-credentials.json';
		}

		$this->load->library( 'upload' );
		$this->upload->initialize( $config );

		$received = $this->upload->do_upload( $field_name );

		if ( !$received ) {
			if( !empty( $this->upload->error_msg ) ) {
				$this->setError( array_slice( $this->upload->error_msg, -1 )[0] );
			} else {
				$this->setError( 'An unknown error occurred.' );
			}

			// We don't log the error(s) here, because the upload library already logs them as they're set.
			return false;
		}

		$upload_data = $this->upload->data();

		$file = json_decode( @file_get_contents( $upload_data['full_path'] ) );

		// really basic validation
		if ( !isset( $file->client_email ) || !isset( $file->private_key ) ) {
			$this->setError( "Uploaded file is not the correct file." );
			@unlink( $upload_data['full_path'] );

			return false;
		}

		return $this->upload->data();
	}

	protected function transformData( array $data )
	{
		$this->load->library( 'encryption' );

		$fields = $this->getFields( $data['id'] );

		$transformed = [];
		$mapped_field_ids = [];

		foreach ( $fields as $key => $field )
		{
			$mapped_field_ids[$field['name']] = $field['id'];
		}

		$data = array_diff_key( $data, array_flip( [ 'id', 'pk_file' ] ) );

		foreach ( $data as $key => $value )
		{
			if ( empty( $value ) || is_null( $value ) ) {
				log_message( "info",
					__METHOD__ . ": {$key} value was null or empty."
				);
				continue;
			}
			
			if ( isset( $mapped_field_ids[$key] ) ) {
				$transformed[$key]['id'] = $mapped_field_ids[$key];
				// encrypt passwords
				$transformed[$key]['value'] = strpos( $key, 'password' ) !== false ?  $this->encryption->encrypt( $value ) : $value;
			}
		}

		return array_values( $transformed );
	}

	protected function transformResult( array $rows )
	{
		$transformed = [];

		foreach ( $rows as $key => $row )
		{
			$row['id'] = (int) $row['id'];

			$row['fields'] = $this->services->getFields( $row['id'] );

			$transformed[$row['id']] = $row;
		}

		return $transformed;
	}

}
