<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Room_model extends IW_Model
{
	/** @var string */
	protected $table = 'rooms';

	public function delete( $id )
	{
		$deleted = parent::delete( $id );

		if ( $deleted ) {
			$this->load->driver( 'cache', [ 'adapter' => 'file' ] );
			$this->cache->delete( "events/{$id}" );
		}

		return $deleted;
	}

	public function create( array $data )
	{
		$created = parent::create( $data );

		if ( !$created ) {
			return false;
		}

		$exported = $this->exportAsset( $created );

		if ( $exported )
		{
			$updated = $this->db->update( $this->table, [ 'cchd_record' => $exported ], [ 'id' => $created['id'] ] );	
		}

		return $created;
	}

	public function update( $id, array $data )
	{
		$room = $this->get( $id );

		$updated = parent::update( $id, $data );

		if ( $updated === false ) {
			return false;
		}

		$exported = $this->exportAsset( $updated );

		if ( $exported ) {
			$updated = $this->db->update( $this->table, [ 'cchd_record' => $exported ], [ 'id' => $id ] );

			if ( $updated === false ) {
				$error = $this->db->error();
				$this->setError( $error['message'], $error['code'] );

				return false;
			}
		}

		if ( (int)$room['service_id'] !== (int)$updated['service_id'] ) {
			$this->load->driver( 'cache', [ 'adapter' => 'file' ] );
			$this->cache->delete( "events/{$room['id']}" );
		}

		return $this->get( $id );
	}

	/**
	 * @param array $data
	 *
	 * @return array
	 */
	protected function transformData( array $data )
	{
		$transformed = [];

		$data = array_intersect_key( $data, array_flip( [
			'name', 'email', 'device_id', 'theme_id', 'service_id', 'group', 'hidden', 'view_all'
		] ) );

		$isInt = [ 'service_id', 'device_id', 'theme_id', 'group', 'hidden', 'view_all' ];

		foreach ( $data as $key => $value )
		{
			$transformed[$key] = in_array( $key, $isInt ) ? (int)$value : $value;
		}

		return $transformed;
	}

	private function exportAsset( array $asset )
	{
		$this->load->model( 'settings_model', 'settings' );

		$this->load->library( "CCHD_Media", [
			"account" 	=> $this->config->item( 'cchd_account' ),
			"user" 		=> $this->config->item( 'cchd_user' ),
			"password" 	=> $this->config->item( 'cchd_password' ),
			"timeout" 	=> $this->config->item( "cchd_api_timeout" )
		]);

		$single_asset = $this->settings->get( 'single_cchd_asset' );

		if ( $single_asset ) {
			$record = $this->settings->get( 'cchd_record_id' );
			$exported = $this->cchd_media->exportRemote( "Booking App", base_url( "app" ), empty( $record ) ? null : $record );
			
			if ( $exported ) {
				$this->settings->update( 'cchd_record_id', [ 'value' => $exported ] );
			} else {
				log_message( "error", __METHOD__ . ": Failed to export single html asset to CCHD." );
			}

			return $exported;
		}

		$exported = $this->cchd_media->exportRemote( "{$asset['name']} - Booking App", base_url( "app/#/room/{$asset['id']}" ), $asset['cchd_record'] );

		if ( $exported ) {
			return $exported;
		} else {
			log_message( "error", __METHOD__ . ": Room #{$asset['id']} failed to export to cchd." );
			return false;
		}
	}

	/**
	 * @param array $result
	 *
	 * @return array
	 */
	protected function transformResult( array $result )
	{
		$transformed = [];

		$isInt = [ 'id', 'device_id', 'theme_id', 'service_id', 'group', 'hidden', 'view_all' ];
		$isBool = [ 'hidden', 'view_all' ];

		foreach( $result as $row ) {
			foreach ( $isInt as $key )
			{
				if ( isset( $row[$key] ) ) {
					$row[$key] = (int)$row[$key];
				}
			}

			foreach ( $isBool as $key )
			{
				if ( isset( $row[$key] ) ) {
					$row[$key] = (int)$row[$key] === 1;
				}
			}
			
			$transformed[$row['id']] = $row;
		}

		return $transformed;
	}
}
