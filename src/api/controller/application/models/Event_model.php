<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Event_model extends IW_Model
{
	public function get( $id = null )
	{
		$this->load->model( 'Room_model', 'rooms' );
		$this->load->model( 'Service_model', 'services' );
		$this->load->helper( 'service' ); //serviceNameFromId

		$response = [];

		if ( !is_null( $id ) ) {
			$room = $this->rooms->get( $id );

			$rooms = [ $room ];
		} else {
			$rooms = $this->rooms->get();
		}

		foreach ( $rooms as $key => $room )
		{
			$service_name =  serviceNameFromId( $room['service_id'] );
			
			$config  = $this->getConfig( $room );

			$this->load->library( "services/{$service_name}", null, $service_name );
			$this->$service_name->init( $config );

			$events = $this->$service_name->getTodaysEvents();

			// set the room's events to empty if the service library returns an error
			$response[$room['id']] = is_array( $events ) ? $events : [];
		}

		return is_null( $id ) ? $response : $response[$id];
	}

	public function create( array $data )
	{
		$this->load->model( "Room_model", "rooms" );
		$this->load->model( "Service_model", "services" );
		$this->load->helper( "service" );

		$room 			= $this->rooms->get( $data['room_id'] );
		$service_name 	= serviceNameFromId( $room['service_id'] );
		$config 		= $this->getConfig( $room );

		$this->load->library( "services/{$service_name}", null, "service" );
		$this->service->init( $config );

		$data = $this->transformData( $data );

		$this->load->model( "Settings_model", "settings" );
		$queue_event_creation = $this->settings->get( "queue_event_creation" );

		// delay the event creation if we have queue_event_creation enabled.
		if ( $queue_event_creation ) {
			$this->load->library( "QueueManager", null, "queue" );

			$this->queue->add([
					"class" 	=> "Events",
					"function" 	=> "delayed_create",
					"filename" 	=> "Events.php",
					"filepath"  => "controllers/api/",
					"type" 		=> "delayedEventCreation",
					"params" 	=> [ $service_name, $config, $data ]
				]);
			
			$this->load->driver( 'cache', [ 'adapter' => 'file' ] );
			
			if ( !$cache = $this->cache->get( $this->service->cacheConfig['file'] ) ) {
				$cache = [];
			}

			$cache[] = $data;

			$this->load->helper( "directory" );
			create_dir( $this->config->item( "cache_path" ) . "events" );

			$this->cache->save( $this->service->cacheConfig['file'], $cache, $this->service->cacheConfig['duration'] );

			return true;
		}

		$created = $this->service->createEvent( $data );

		if ( $created === false ) {
			// Set the error here at the model level so the controller calling this model has access to it
			$error = $this->service->getLastError();
			$this->setError( $error, 500 );
			return false;
		}

		return true;
	}

	private function getConfig( $room ) 
	{
		$this->load->model( "service_model", "services" );
		$config = $this->services->getFieldValues( $room['service_id'], true );
		$config['room_id']		= $room['id'];
		$config['room_name']	= $room['name'];
		$config['room_email'] 	= $room['email'];

		return $config;
	}


	public function transformRequest( array $result ) 
	{
		foreach ( $result as $key => $row )
		{
			$result[$key]['status'] = "existing";
		}

		return $result;
	}

	public function transformData( array $data ) 
	{
		$data = array_intersect_key( $data, array_flip( [
			'title', 'start', 'duration', 'end', 'room_id'
		] ) );

		return $data;
	}
}
