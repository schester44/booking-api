<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Theme_model extends IW_Model
{
	protected $table = 'themes';

	public function create( array $data )
	{
		$default = $this->get( 1 );

		$data['properties'] = $default['properties'];

		$created = parent::create( $data );

		return $created;
	}

	protected function processFiles( $files )
	{
		if ( is_null( $files ) || empty( $files ) ) {
			return true;
		}

		foreach ( $files as $key => $file )
		{
			$uploaded = $this->uploadFile( $key );

			if ( !$uploaded ) {
				return false;
			}
		}

		return true;
	}

	private function uploadFile( $field )
	{
		$config = [
			'allowed_types'    	=> [ 'jpg', 'png', 'gif'],
			'file_ext_tolower' 	=> true,
			'overwrite'        	=> true,
			'upload_path'      	=> $this->config->item( 'data_path' ),
			'file_name'		 	=> "{$field}.png"
		];

		$this->load->library( 'upload' );
		$this->upload->initialize( $config );

		$received = $this->upload->do_upload( $field );

		if ( !$received ) {
			if( !empty( $this->upload->error_msg ) ) {
				$this->setError( array_slice( $this->upload->error_msg, -1 )[0] );
			} else {
				$this->setError( 'An unknown error occurred while attempting to upload your images.' );
			}

			return false;
		}

		$upload_data = $this->upload->data();

		return $this->upload->data();
	}

	protected function transformData( array $data )
	{
		foreach ( $data as $key => $value )
		{
			if ($key === "properties") {
				$data['properties'] = json_encode( $value );
			}

			if ( $key === "images" ) {
				foreach ( $data['images'] as $type => $id )
				{
					if (!is_array( $id ) ) {
						$data["{$type}_id"] = $id;
					} else {
						$data["{$type}_id"] = $id['id'];
					}
				}
			}
		}

		$data = array_intersect_key( $data, array_flip([ 'name', 'properties', 'logo_id', 'background_id' ] ) );
		return $data;
	}
	
	protected function transformResult( array $results )
	{	
		$this->load->model( 'image_model', 'images' );
		$transformed = [];

		foreach( $results as $row )
		{
			$row['id']				= (int)$row['id'];
			$row['logo_id']			= $row['logo_id'] ? (int)$row['logo_id'] : $row['logo_id'];
			$row['background_id']	= $row['background_id'] ? (int)$row['background_id'] : $row['background_id'];
			$row['properties']		= json_decode( $row['properties'], true );
			$row['images'] 			= []; 

			if ( $row['logo_id'] ) {
				$logo = $this->images->get( $row['logo_id'] );
				
				$row['images']['logo'] = $this->images->transformOutput( $logo );
			}

			if ( $row['background_id'] ) {
				$background = $this->images->get( $row['background_id'] );

				if ( is_array( $background ) ) {
					$row['images']['background'] = $this->images->transformOutput( $background );
				}
			}

			$transformed[$row['id']] = $row;
		}

		return $transformed;
	}
}