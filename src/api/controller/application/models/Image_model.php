<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Images_model
 *
 * @property CI_Config    $config
 * @property CI_Loader    $load
 * @property CI_Upload    $upload
 * @property IW_Image_lib $image
 */
class Image_model extends IW_Model
{
	/** @var string */
	protected $table = 'images';

	/** @var string */
	private $images_dir;

	/** @var string */
	private $thumbnails_dir;

	/**
	 * constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->images_dir     = $this->config->item( 'images_dir' );
		$this->thumbnails_dir = $this->config->item( 'images_dir' ) . 'thumbnails/';

		// Make sure the thumbnails directory exists. This will also create the parent
		// images directory, if necessary.
		$this->load->helper( 'directory' );
		create_dir( $this->thumbnails_dir );
		create_dir( $this->images_dir );

		$this->load->library( 'IW_Image_lib', [
			'convert_path' => $this->config->item( 'convert_path' )
		], 'image' );
	}

	/**
	 * @param $category
	 *
	 * @return array|false
	 */
	public function getByCategory( $category )
	{
		$this->db->select()
		         ->from( $this->table )
		         ->where( 'category', $category );

		$result = $this->db->get();

		if( $result === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return $this->transformResult( $result->result_array() );
	}

	/**
	 * @param array $data
	 *
	 * @return array|false The created image on success, or FALSE on failure.
	 */
	public function create( array $data )
	{
		$upload_data = $this->receiveImage( 'file' );

		if( $upload_data === false ) {
			return false;
		}

		// Save the image meta data to the database.
		$image = parent::create( [
			'name'     => @$data['name'] ?: pathinfo( $upload_data['orig_name'], PATHINFO_FILENAME ),
			'category' => @$data['category'],
			'filename' => $upload_data['file_name']
		] );

		if( $image === false ) {
			unlink( $upload_data['full_path'] );
			unlink( $upload_data['thumbnail_full_path'] );

			return false;
		}

		return $image;
	}

	/**
	 * @param int   $id
	 * @param array $data
	 *
	 * @return array|false
	 */
	public function update( $id, array $data )
	{
		$data = $this->transformData( $data );

		$image = $this->get( $id );

		if( isset( $_FILES['file'] ) ) {
			$upload_data = $this->receiveImage( 'file' );

			if( $upload_data === false ) {
				return false;
			}

			$data['filename'] = $upload_data['file_name'];
		}

		if( !empty( $data ) ) {
			$updated = parent::update( $id, $data );

			if( $updated === false ) {
				$error = $this->db->error();
				$this->setError( $error['message'], $error['code'] );

				return false;
			}

			// If a new image was uploaded, delete the files for the old one.
			if( isset( $upload_data ) ) {
				unlink( $image['filepath'] );
				unlink( $image['thumbnail_filepath'] );
			}
		}

		return $this->get( $id );
	}

	/**
	 * @param int $id
	 *
	 * @return array|null|false The deleted image on success, NULL if the image
	 *                          could not be found, or FALSE on failure.
	 */
	public function delete( $id )
	{
		// Delete the database entry.
		$image = parent::delete( $id );

		if( $image === false || $image === null ) {
			return $image;
		}

		// Delete the actual file.
		$file_deleted = @unlink( $image['filepath'] );

		if( !$file_deleted ) {
			log_message( 'error',
				__METHOD__ . ": Failed to delete image '{$image['filename']}': " . error_get_last()['message']
			);
		}

		// Delete the thumbnail.
		$thumbnail_deleted = @unlink( $image['thumbnail_filepath'] );

		if( !$thumbnail_deleted ) {
			log_message( 'error',
				__METHOD__ . ": Failed to delete thumbnail '{$image['filename']}': " . error_get_last()['message']
			);
		}

		return $image;
	}

	/**
	 * @param string $pathname
	 *
	 * @return string|false
	 */
	public function createThumbnail( $pathname )
	{
		if( !file_exists( $pathname ) ) {
			log_message( 'error',
				__METHOD__ . ": '{$pathname}' does not exist"
			);

			return false;
		}

		$thumbnail_pathname = $this->thumbnails_dir . basename( $pathname );

		// Make a copy of the image, which we'll use as the thumbnail.
		$copied = @copy( $pathname, $thumbnail_pathname );

		if( !$copied ) {
			$error = error_get_last();
			log_message( 'error',
				__METHOD__ . ": Failed to copy '{$pathname}' to '{$thumbnail_pathname}': {$error['message']}"
			);

			return false;
		}

		// Shrink the copied image down to thumbnail size.
		$resized = $this->image->shrink( $thumbnail_pathname, 192, 192 );

		if( $resized === false ) {
			$error = error_get_last();
			log_message( 'error',
				__METHOD__ . ": Failed to resize '{$thumbnail_pathname}': {$error['message']}"
			);

			unlink( $thumbnail_pathname );

			return false;
		}

		return $thumbnail_pathname;
	}

	/**
	 * @param array $output
	 * @param bool  $multiple      Set to TRUE if the response contains multiple
	 *                             images, FALSE if it's a single image.
	 * @param bool  $preserve_keys If TRUE, and $multiple is also TRUE, will keep
	 *                             the array indexes of the original array.
	 *
	 * @return array
	 */
	public function transformOutput( array $output, $multiple = false, $preserve_keys = false )
	{
		if( $multiple ) {
			$transformed = [];
			foreach( $output as $key => $value ) {
				if( $preserve_keys ) {
					$transformed[$key] = $this->transformOutput( $value );
				} else {
					$transformed[] = $this->transformOutput( $value );
				}
			}
		} else {
			log_message( 'debug',
				__METHOD__ . ': Before: ' . var_export( $output, true )
			);

			$transformed = array_intersect_key( $output, array_flip( [
				'id', 'name', 'category'
			] ) );

			$transformed['url']           = site_url( "images/{$transformed['id']}" );
			$transformed['thumbnail_url'] = "{$transformed['url']}/thumbnail";

			log_message( 'debug',
				__METHOD__ . ': After: ' . var_export( $transformed, true )
			);
		}

		return $transformed;
	}

	/**
	 * @param string $file_field_name
	 * @param string   $filename
	 *
	 * @return array|false
	 */
	private function receiveImage( $file_field_name, $filename = null )
	{
		if( !is_dir( $this->images_dir ) ) {
			log_message( 'error',
				__METHOD__ . ": '{$this->images_dir}' does not exist or is not a directory"
			);

			$this->setError( 'The ' . basename( $this->images_dir ) . ' directory seems to be missing.' );
			return false;
		}

		$this->load->library( 'upload' );
		$this->upload->initialize( [
			'allowed_types'    => [ 'jpg', 'jpeg', 'png', 'svg' ],
			'encrypt_name'     => true,
			'file_ext_tolower' => true,
			'overwrite'        => true,
			'upload_path'      => $this->images_dir
		] );

		$received = $this->upload->do_upload( $file_field_name );

		if( !$received ) {
			if( !empty( $this->upload->error_msg ) ) {
				$this->setError( array_slice( $this->upload->error_msg, -1 )[0] );
			} else {
				$this->setError( 'An unknown error occurred.' );
			}

			// We don't log the error(s) here, because the upload library already logs them
			// as they're set.

			return false;
		}

		$upload_data = $this->upload->data();

		if( $upload_data === false ) {
			return false;
		}

		$thumbnail_pathname = $this->createThumbnail( $upload_data['full_path'] );

		// Shrink the original image down to some reasonable size.
		$resized = $this->image->shrink( $upload_data['full_path'], 1920, 1920 );

		if( $resized === false ) {
			unlink( $upload_data['full_path'] );
			unlink( $thumbnail_pathname );

			$this->setError( 'Failed to resize the image.' );
			return false;
		}

		$upload_data['thumbnail_full_path'] = $thumbnail_pathname;

		return $upload_data;
	}

	/**
	 * @param array $data
	 *
	 * @return array
	 */
	protected function transformData( array $data )
	{
		return array_intersect_key( $data, array_flip( [
			'name', 'category', 'filename'
		] ) );
	}

	/**
	 * @param array $result
	 *
	 * @return array
	 */
	protected function transformResult( array $result )
	{
		log_message( 'debug',
			__METHOD__ . ': Before: ' . var_export( $result, true )
		);

		$transformed = [];

		foreach( $result as $row ) {
			$row['id']                 = (int)$row['id'];
			$row['filepath']           = $this->images_dir . $row['filename'];
			$row['thumbnail_filepath'] = $this->thumbnails_dir . $row['filename'];

			$transformed[$row['id']] = $row;
		}

		log_message( 'debug',
			__METHOD__ . ': After: ' . var_export( $transformed, true )
		);

		return $transformed;
	}
}