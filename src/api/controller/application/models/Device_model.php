<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * Devices_model
 *
 * @property CI_Cache  $cache
 * @property CI_Config $config
 * @property CI_Loader $load
 */
class Device_model extends IW_Model
{
	/**
	 * @param int $id
	 *
	 * @return array|false
	 */
	public function get( $id = null )
	{
		$this->load->driver( 'cache', [
			'adapter' => 'file'
		] );

		$cache_id = 'devices';

		$devices = $this->cache->get( $cache_id );

		if( $devices === false ) {
			$ch = curl_init( "https://cchdrsrc.channelshd.com/modules/app-endpoints/getDevices.php?account={$this->config->item( 'cchd_account' )}&token=An6iDg6UP2mDW3Ob" );
			curl_setopt_array( $ch, [
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false
			] );

			$json = curl_exec( $ch );

			$curl_error = curl_error( $ch );
			$http_code  = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

			curl_close( $ch );

			if( $curl_error !== '' ) {
				log_message( 'error',
					__METHOD__ . ": Failed to get devices: {$curl_error}"
				);

				$this->setError( $curl_error );
				return false;
			}

			if( $http_code !== 200 ) {
				log_message( 'error',
					__METHOD__ . ": Failed to get devices: HTTP {$http_code}"
				);

				$this->setError( "HTTP {$http_code}" );
				return false;
			}

			$devices = json_decode( $json, true );

			if( isset( $devices['status'] ) && strtolower( $devices['status'] ) === 'error' ) {
				log_message( 'error',
					__METHOD__ . ": {$devices['message']}"
				);

				$this->setError( $devices['message'] );
				return false;
			}

			$this->cache->save( $cache_id, $devices, 3600 );
		}

		return $this->transformResult( $devices );
	}

	/**
	 * @param array $result
	 *
	 * @return array
	 */
	protected function transformResult( array $result )
	{
		log_message( 'debug',
			__METHOD__ . ': Before: ' . var_export( $result, true )
		);

		$transformed = [];

		foreach( $result as $row ) {
			$row['id'] = (int)$row['id'];

			$transformed[$row['id']] = $row;
		}

		log_message( 'debug',
			__METHOD__ . ': After: ' . var_export( $transformed, true )
		);

		return $transformed;
	}
}