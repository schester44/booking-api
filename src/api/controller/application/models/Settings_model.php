<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Settings_model extends IW_Model
{
	/** @var string */
	protected $table = 'settings';

	public function get( $name = null )
	{
		$this->db->select()->from( $this->table );

		if ( !is_null ( $name ) ) {
			$this->db->where( is_array( $name ) ? $name : array( 'name' => $name ) );
		}

		$result = $this->db->get();

		if ( $result === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		$rows = $this->transformOutput( $result->result_array() );

		return !is_null( $name ) && !is_array( $name ) ? $rows[$name] : $rows;
	}


	public function update( $id, array $data )
	{
		$updated = $this->db->update( $this->table, $this->transformData( $data ), [ 'name' => $id ] );

		if ( $updated === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return $this->get( $id );
	}

	/**
	 * Update settings
	 * @param  [null] $null -- here to satisfy the IW_Model requirements
	 * @param  array  $data - key/value array of settigns
	 * @return [bool] 
	 */
	public function batchUpdate( array $data )
	{

		$updated = $this->db->update_batch( $this->table, $this->transformInput( $data ), 'name' );

		if ( $updated === false ) {
			$error = $this->db->error();
			$this->setError( $error['message'], $error['code'] );

			return false;
		}

		return true;
	}

	protected function transformInput( $input )
	{
		$data = array_intersect_key( $input, array_flip([
				'queue_event_creation', 'cchd_record_id', 'single_cchd_asset', 'timezone'
			]));

		$transformed = [];

		foreach ( $data as $key => $value )
		{
			$transformed[] = [ 'name' => $key, 'value' => $value ];
		}

		return $transformed;
	}

	protected function transformOutput( array $result )
	{
		$transformed = [];

		$isInt = [ "queue_event_creation", "single_cchd_asset" ];
		$isBool = [ "queue_event_creation", "single_cchd_asset" ];
		
		foreach ( $result as $key => $row )
		{	
			if ( in_array( $row['name'], $isInt ) ) {
				$row['value'] = (int) $row['value'];
			}

			if ( in_array( $row['name'], $isBool ) ) {
				$row['value'] = ($row['value'] === 1);
			}

			$transformed[$row['name']] = $row['value'];
		}

		return $transformed;
	}
}