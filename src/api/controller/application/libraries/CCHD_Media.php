<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

use CCHD\Media\API as MediaApi;
use CCHD\Media\Record as MediaRecord;

class CCHD_Media
{
	private $media;

	function __construct( array $parameters = [] )
	{
		$this->media = new MediaApi(
			$parameters['account'],
			$parameters['user'],
			$parameters['password'],
			[
				MediaApi::CONFIG_TIMEOUT => $parameters['timeout']
			]
		);
	}

	public function exportRemote( $name, $url, $cchd_record = null )
	{
		$create_new = true;

		if ( $cchd_record !== "null" && $cchd_record !== null ) {
			$create_new = false;

			$response = $this->media->editRemoteHtml( $cchd_record, $url, [
				MediaApi::PARAM_NAME => $name
			] );

			if ( $response === false ) {
				$last_response = $this->media->getLastResponse();

				// If the object ID we have saved for this menu no longer exists, we need to
				// create a new asset. Consider all other errors fatal.
				if ( $last_response['message'] === 'media not found' ) {
					log_message( 'error', sprintf( "%s: The media asset for '{$name}' could not be found; creating a new asset", __METHOD__ ) );
					$create_new = true;
				} else {
					log_message( 'error', sprintf( "%s: Failed to update '{$name}': {$last_response['message']}", __METHOD__ ) );
					return false;
				}
			}
		}

		if ( $create_new ) {
			$response = $this->media->addRemoteHtml( $url, $name, [ MediaApi::PARAM_APP_FLAG => true ] );

			if ( $response === false ) {
				$last_response = $this->media->getLastResponse();
				log_message( 'error', sprintf( "%s: Failed to upload '{$name}': {$last_response['message']}", __METHOD__ ) );

				return false;
			}
		}

		return $response[MediaRecord::OBJECT_ID];
	}

	public function deleteRemote( $object_id )
	{
		$deleted = $this->media->deleteRemote( $object_id );

		if ( !$deleted ) {
			log_message( "error", __METHOD__ . ": The media asset could not be deleted. ({$object_id})" );
		}

		return $deleted;
	}
}