<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * AppControl
 *
 * @property CI_Config           $config
 * @property CI_DB_query_builder $db
 * @property CI_Loader           $load
 * @property IW_Output           $output
 */
class AppControl
{
	public function install()
	{
		// Set the output way up here so that we can return early in case of an error
		// and not put the app system into an infinite install loop.
		$this->output->set_output( '$OK' );

		$queries = @file_get_contents( APPPATH . 'sql/install.sql' );

		if( $queries === false ) {
			log_message( 'error',
				__METHOD__ . ': Failed to read the SQL file: ' . error_get_last()['message']
			);

			return;
		}

		$result = $this->db->query( $queries );

		if( $result === false ) {
			log_message( 'error',
				__METHOD__ . ': Failed to execute SQL queries: ' . $this->db->error()['message']
			);

			return;
		}
	}

	public function sync()
	{
	}

	public function upgrade()
	{
		// Set the output way up here so that we can return early in case of an error
		// and not put the app system into an infinite upgrade loop.
		$this->output->set_output( '$OK' );
	}

	public function remove()
	{
		$this->load->model( 'settings_model', 'settings' );

		$record = $this->settings->get( 'cchd_record_id' );

		if ( !empty( $record ) ) {			
			$this->load->library( "CCHD_Media", [
				"account" 	=> $this->config->item( 'cchd_account' ),
				"user" 		=> $this->config->item( 'cchd_user' ),
				"password" 	=> $this->config->item( 'cchd_password' ),
				"timeout" 	=> $this->config->item( "cchd_api_timeout" )
			]);

			$this->ccd_media->deleteRemote( $record );
		}

		$this->output->set_output( '$OK' );
	}

	/**
	 * @param string $name
	 *
	 * @return object
	 */
	public function __get( $name )
	{
		return get_instance()->$name;
	}
}
