<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * IW_Image_lib
 */
class IW_Image_lib
{
	/** @var string */
	private $convert_path;

	/**
	 * constructor
	 *
	 * @param array $parameters
	 */
	public function __construct( array $parameters )
	{
		$this->convert_path = @$parameters['convert_path'] ?: 'convert';
	}

	/**
	 * Shrinks an image down to the specified width and height, while maintaining
	 * aspect ratio.
	 *
	 * @param string $pathname The full path to the image.
	 * @param int    $width    The maximum width of the final image.
	 * @param int    $height   The maximum height of the final image.
	 *
	 * @return bool|null TRUE on success, FALSE on failure, and NULL if nothing was done.
	 */
	public function shrink( $pathname, $width = 1920, $height = 1920 )
	{
		// If the image is within the set dimensions, we don't need to do anything.
		$info = getimagesize( $pathname );
		if( $info[0] <= $width && $info[1] <= $height ) {
			return null;
		}

		$tmp_pathname = "{$pathname}~";

		// Resize the image to the maximum width and height specified. Slightly modified
		// from: https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/
		exec( sprintf( '%s %s -filter Triangle -define filter:support=2 -thumbnail %s -unsharp 0.25x0.25+8+0.065 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip %s 2>&1',
			$this->convert_path,
			escapeshellarg( $pathname ),
			escapeshellarg( "{$width}x{$height}>" ),
			escapeshellarg( $tmp_pathname )
		), $output, $exit_code );

		if( !file_exists( $tmp_pathname ) ) {
			return false;
		}

		if( $exit_code !== 0 ) {
			unlink( $tmp_pathname );
			return false;
		}

		// Replace the original image with the resized one.
		$renamed = rename( $tmp_pathname, $pathname );

		if( !$renamed ) {
			unlink( $tmp_pathname );
			return false;
		}

		return true;
	}
}