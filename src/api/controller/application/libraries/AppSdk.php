<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * AppSdk
 *
 * @method bool authenticate()
 * @method string getAccountID()
 * @method array getAllEnv()
 * @method string getAppCode()
 * @method string getCCHDEnables()
 * @method string getCCHDSession()
 * @method array getCCHDToken()
 * @method string getCCHDVersion()
 * @method string getControlCommand()
 * @method string getDevCode()
 * @method string getEnv( string $variableName )
 * @method array|false getNetwork()
 * @method string getRole()
 * @method string getSyncFilePath()
 * @method string getSyncHash()
 * @method string getZoneID()
 * @method bool isSandbox()
 * @method setClean( string $syncHash )
 * @method setDirty()
 * @method string validateCCHDSession( string $session, string $sessionid, string $userptr )
 */
class AppSdk
{
	/** @var CCHDAASDK */
	private $sdk;

	/**
	 * constructor
	 */
	public function __construct()
	{
		if( file_exists( CCHDAASDK_PATH ) ) {
			require_once CCHDAASDK_PATH;

			$this->sdk = new CCHDAASDK();
		}
	}

	/**
	 * @param string $name
	 * @param array  $arguments
	 * @return mixed
	 */
	public function __call( $name, array $arguments )
	{
		if( $this->sdk instanceof CCHDAASDK ) {
			return call_user_func_array( [ $this->sdk, $name ], $arguments );
		}

		return null;
	}
}
