<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

/**
 * AppUi
 *
 * @property AppSdk    $appsdk
 * @property CI_Loader $load
 */
class AppUi
{
	/**
	 * @param string $session
	 * @param string $session_id
	 * @param string $user
	 *
	 * @return bool
	 */
	public function validateSession( $session, $session_id, $user )
	{
		if( !isset( $session, $session_id, $user ) ) {
			return false;
		}

		$this->load->library( 'AppSdk' );

		$response_parts = explode( "\n", $this->appsdk->validateCCHDSession( $session, $session_id, $user ) );

		if( strpos( $response_parts[0], '$OK' ) === false ) {
			return false;
		}

		$session_parts = explode( ':', $response_parts[1] );

		$this->load->library( 'session' );

		$_SESSION['user_id']       = $session_parts[0];
		$_SESSION['default_group'] = $session_parts[1];
		$_SESSION['groups']        = $session_parts[2];

		return true;
	}

	/**
	 * @param string $name
	 *
	 * @return object
	 */
	public function __get( $name )
	{
		return get_instance()->$name;
	}
}
