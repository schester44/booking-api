<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Offline
{
	private $libConfig;
	public $cacheConfig;
	protected $error = null;

	function __construct()
	{
		$this->load->driver( 'cache', [ 'adapter' => 'file' ] );
	}

	public function init( array $config )
	{
		$this->libConfig = $config;

		$this->cacheConfig = [
			"file" => "events/{$this->libConfig['room_id']}",
			"duration" => 86400 // 86400 = one day -- a cron task will delete all cache at midnight so theoretically this could be 10 years and the outcome should be the same.
		];
	}

	public function getTodaysEvents()
	{
		$cache = $this->cache->get( $this->cacheConfig['file'] );

		return !empty( $cache ) ? $cache : [];
	}

	public function createEvent( array $input )
	{
		if ( !$cache = $this->cache->get( $this->cacheConfig['file'] ) ) {
			$cache = [];
		}

		$cache[] = $input;

		$this->load->helper( "directory" );
		create_dir( $this->config->item( "cache_path" ) . "events" );
		
		$this->cache->save( $this->cacheConfig['file'], $cache, $this->cacheConfig['duration'] );

		return $input;
	}

	public function getLastError() {
		return $this->error;
	}

	public function setError( $msg )
	{
		$this->error = $msg;
		return $this;
	}

	public function __get( $name ) { return get_instance()->$name; }
}