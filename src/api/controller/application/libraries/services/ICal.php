<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class ICal
{
	public $cacheConfig;

	protected $error = null;
	private $libConfig;

	function __construct()
	{

		$this->load->driver( 'cache', [ 'adapter' => 'file' ] );
		$this->credentials = $this->config->item( "data_path" ) . "gcal-credentials.json" ;
	}

	public function init( array $config )
	{
		$this->libConfig = $config;
		
		$this->cacheConfig = [
			"file" => "events/{$this->libConfig['room_id']}",
			"duration" => 300
		];

		$this->load->library('IWGCal', null, 'client' );

		$config = [
			'credentials' 	=> $this->credentials,
			'calendarId' 	=> $config['service_acct_calendar'],
			'room_email' 	=> $this->libConfig['room_email']
		];

		$this->client->init( $config );
	}

	public function getTodaysEvents( $useCache = true )
	{
		$credsExist = $this->checkCredentials();

		if ( !$credsExist ) return false;

		if ( $useCache ) {
			$cache = $this->cache->get( $this->cacheConfig['file'] );

			if ( $cache && !empty( $cache ) ) {
				return $cache;
			}
		}

		$parameters = [
			'singleEvents' 	=> true,
			'timeZone' 		=> date_default_timezone_get(),
			'timeMin' 		=> date( 'Y-m-d' ) . 'T00:00:00Z',
			'timeMax' 		=> date( 'Y-m-d' ) . 'T23:59:59Z',
			'orderBy' 		=> 'startTime'
		];

		try {
			$events = $this->client->getEvents( $parameters );
		} catch  (Exception $e ) {
			$error = json_decode( $e->getMessage() );
			$this->setError( $error->error->message );
			return false;
		}

		$events = $this->transformOutput( $events );

		if ( !empty( $events ) ) {
			if ( $useCache ) {
				$this->createCacheDir();
				$this->cache->save( $this->cacheConfig['file'], $events, $this->cacheConfig['duration'] );
			}
		}

		return $events;
	}

	public function createEvent( array $input )
	{
		$credsExist = $this->checkCredentials();

		if ( !$credsExist ) return false;

		$eventData = $this->transformData( $input );

		try {
			$created = $this->client->createEvent( $eventData );
		} catch( Exception $e ) {
			$error = json_decode( $e->getMessage() );
			$message = $error->error->message;

			$message = strtolower($message) === "forbidden" ? "You do not have permission to create this event." : $message;

			$this->setError( $message );
			return false;
		}

		if ( !$created ) {
			log_message( "error",
				__METHOD__ . "Failed to create an event. Config: " . var_export( $this->libConfig )
			);
			return false;
		}

		$cache = $this->cache->get( $this->cacheConfig['file'] );

		if ( !$cache = $this->cache->get( $this->cacheConfig['file'] ) ) {
			$cache = [];
		}

		$cache[] = $input;

		$this->createCacheDir();
		$this->cache->save( $this->cacheConfig['file'], $cache, $this->cacheConfig['duration'] );

		return true;
	}

	public function getLastError() {
		return $this->error;
	}

	public function setError( $msg )
	{
		$this->error = $msg;
		return $this;
	}

	private function transformOutput( array $events )
	{
		$this->load->helper( "time" );
		$transformed = [];

		$calendar = $this->client->calendarInfo();

		foreach ( $events as $key => $event )
		{

			$startTime 	= new DateTime( date( 'Y-m-d H:i:s', strtotime( $event->getStart()->dateTime ) ), new DateTimeZone( 'UTC' ) );
			$endTime 	= new DateTime( date( 'Y-m-d H:i:s', strtotime( $event->getEnd()->dateTime ) ), new DateTimeZone( 'UTC' ) );

			$startTime->setTimezone( new DateTimeZone( $calendar->timeZone ) );
			$endTime->setTimezone( new DateTimeZone( $calendar->timeZone ) );

			$start = (int)$startTime->format( 'H' ) + ( (int)$startTime->format('i') / 60 );
			$end   = (int)$endTime->format( 'H' ) + ( (int)$endTime->format( 'i' ) / 60 );

			$data = [
				"title" 	=> $event['summary'],
				"status" 	=> "existing",
				"start" 	=> $start,
				"end" 		=> $end,
				"duration" 	=> $end - $start
			];

			$transformed[$key] = $data;
		}

		return $transformed;
	}

	private function transformData( array $data )
	{
		$this->load->helper( 'time' );
		$calendar = $this->client->calendarInfo();

		$startTime 	= new DateTime( date('Y-m-d H:i:s', numberToTimestamp( $data['start'] ) ), new DateTimeZone( $calendar->timeZone ) );
		$endTime 	= new DateTime( date('Y-m-d H:i:s', numberToTimestamp( $data['end'] ) ), new DateTimeZone( $calendar->timeZone ) );

		$startTime->setTimezone( new DateTimeZone( 'UTC' ) );
		$endTime->setTimezone( new DateTimeZone( 'UTC' ) );

		$start 		= $startTime->format('Y-m-d') . 'T' . $startTime->format('H:i:s');
		$end 		= $endTime->format('Y-m-d') . 'T' . $endTime->format('H:i:s');

		$transformed = [
			"summary" => $data["title"],
			"location" => $this->libConfig["room_name"],
			"description" => "Created using the Industry Weapon Booking App",
			"start" => [
				"dateTime" => $start,
				"timeZone" => 'UTC'
			],
			"end" => [
				"dateTime" => $end,
				"timeZone" => 'UTC'
			],
			"attendees" => [
				[ "email" => $this->libConfig["room_email"] ]
			]
		];

		return $transformed;
	}

	private function createCacheDir()
	{
		$this->load->helper( "directory" );
		create_dir( $this->config->item( "cache_path" ) . "events" );
	}

	private function checkCredentials() 
	{
		if ( !file_exists( $this->credentials ) ) {
			$this->setError( "Required credentials file does not exist ({$this->credentials})" );
			return false;
		}

		return true;
	}


	public function __get( $name ) { return get_instance()->$name; }
}