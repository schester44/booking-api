<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class EWS
{
	public $cacheConfig;

	protected $error = null;

	private $client;
	private $libConfig;

	function __construct()
	{
		require_once APPPATH . 'libraries/services/EWS/NTLMStream.php';   	
		require_once APPPATH . 'libraries/services/EWS/NTLMSoapClient.php';   	
		require_once APPPATH . 'libraries/services/EWS/ExchangeClient.php';   	
		require_once APPPATH . 'libraries/services/EWS/ExchangeNTLMStream.php';
		
		$this->load->driver( 'cache', [ 'adapter' => 'file' ] );
		$this->load->helper( "directory" );	
	}

	public function init ( array $config )
	{
		$this->libConfig = $config;

		$this->cacheConfig = [
			"file" 		=> "events/" . $this->libConfig['room_id'],
			"duration"  => 300
			];

		$this->load->library( "encryption" );
		$config['service_acct_password'] = $this->encryption->decrypt( $config['service_acct_password'] );

		$this->client = new ExchangeClient();

		$this->client->init(
			$config['service_acct_email'],
			$config['service_acct_password'],
			$config['room_email'],
			APPPATH . "libraries/services/EWS/services.wsdl"
		);
	}

	public function getTodaysEvents()
	{
		$cache = $this->cache->get( $this->cacheConfig['file'] );

		if ( $cache && !empty( $cache ) ) {
			return $cache;
		}

		$optParams = [
			'start' =>  date( 'Y-m-d' ) . 'T00:00:00',
			'end'  	=> date( 'Y-m-d') . 'T23:59:00'
		];

		try {
			$events = $this->client->getEvents( $optParams['start'], $optParams['end'] );
		} catch( Exception $e ) {
			$error = json_decode( $e->getMessage() );
			$this->setError( $error->error->message );
			return false;
		}

		if ( !$events ) {
			$this->setError( $this->client->lastError && $this->client->lastError->MessageText ? $this->client->lastError->MessageText : null );
			return false;
		}

		$events = $this->transformOutput( $events );

		if ( !empty( $events ) ) {
			create_dir( $this->config->item( "cache_path" ) . "events" );

			$this->cache->save( $this->cacheConfig['file'], $events, $this->cacheConfig['duration'] );
		}

		return $events;
	}

	public function createEvent( array $input )
	{
		$data = $this->transformData( $input );

		try {
			$created = $this->client->create_event( $data['subject'], $data['start'], $data['end'], $data['location'], $data['allday'] );
		} catch( Exception $e ) {
			$error = json_decode( $e->getMessage() );
			$this->setError( $error->error->message );
			return false;	
		}

		if ( $created === false ) {
			$this->setError( "Failed to create the event" );
			return false;
		}

		if ( !$cache = $this->cache->get( $this->cacheConfig['file'] ) ) {
			$cache = [];
		}

		$cache[] = $input;

		create_dir( $this->config->item( "cache_path" ) );
		$this->cache->save( $this->cacheConfig['file'], $cache, $this->cacheConfig['duration'] );

		return true;
	}

	public function getLastError() {
		return $this->error;
	}

	public function setError( $msg )
	{
		$this->error = $msg;
		return $this;
	}

	private function transformOutput( array $events )
	{
		$this->load->helper( "time" );
		$transformed = [];

		$this->load->model( 'Settings_model', 'settings' );
		$timezone = $this->settings->get( 'timezone' ); 

		foreach ( $events as $key => $event )
		{
			$start 	= timestampToNumber( strtotime( $event->start ), $timezone );
			$end 	= timestampToNumber( strtotime( $event->end ), $timezone );

			$data = [
				"title" 	=> $event->subject,
				"status" 	=> "existing",
				"start" 	=> $start,
				"end" 		=> $end,
				"duration" 	=> $end - $start
			];

			$transformed[$key] = $data;
		}

		return $transformed;
	}

	private function transformData( array $data )
	{
		$this->load->helper( 'time' );
		$this->load->model( 'Settings_model', 'settings' );
		$timezone = $this->settings->get( 'timezone' );

		$startTime = new DateTime( date('Y-m-d H:i:s', numberToTimestamp( $data['start'] ) ), new DateTimeZone( $timezone ) );
		$startTime->setTimeZone( new DateTimeZone('UTC' ) );

		$start = $startTime->format('Y-m-d') . 'T' . $startTime->format('H:i') . ':00Z';

		$endTime = new DateTime( date('Y-m-d H:i:s', numberToTimestamp( $data['end'] ) ), new DateTimeZone( $timezone ) );
		$endTime->setTimeZone( new DateTimeZone('UTC' ) );

		$end = $endTime->format('Y-m-d') . 'T' . $endTime->format('H:i') . ':00Z';

		$transformed = [
			"subject" 	=> $data['title'],
			"start" 	=> $start,
			"end" 		=> $end,
			"location" 	=> null,
			"allday" 	=> false
		];

		return $transformed;
	}

	public function __get( $name ) { return get_instance()->$name; }
}