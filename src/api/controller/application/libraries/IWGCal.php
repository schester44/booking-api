<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class IWGCal
{

	private $client;
	private $service;
	private $calendar;
	private $libConfig;

	public function init( array $config )
	{
		if ( !is_array( $config ) ) {
			log_message( "error", __METHOD__ . ": Params are missing or invalid." );
			throw new Exception( "Error Processing Params", 1 );
		}

		// put the service accunt credentials into an env var
		putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $config['credentials'] );

		if ( !isset( $config['credentials'] ) ) {
			log_message( "error", __METHOD__ . ": Credentials location is missing." );
			throw new Exception( "credentials location missing.", 1 );
		} else {
			if ( !file_exists ($config['credentials'] ) ) {
				log_message( "error", __METHOD__ . ": Credentials file does not exist" );
				throw new Exception( "Credentials file does not exist", 1 );
			}
		}

		$this->libConfig = $config;

		$this->calendar = isset( $config['calendarId'] ) ? $config['calendarId'] : null;
		
		$this->client = new Google_Client();
		
		// disable SSL verification to get around CCHD app environment limitations
		$this->client->getHttpClient()->setDefaultOption( 'verify', false );

		$this->client->useApplicationDefaultCredentials();
		
		$this->client->setScopes( [ 'https://www.googleapis.com/auth/calendar' ] );

		$this->service = new Google_Service_Calendar( $this->client );
	}

	public function calendarInfo( $id = null )
	{
		$cal = is_null( $id ) ? $this->calendar : $id;

		return $this->service->calendars->get( $this->calendar );
	}

	public function createCalendar($data = [])
	{
		if ( empty( $data ) || !isset( $data['summary'] ) || !isset( $data['timeZone'] ) ) {
			return false;
		}

		$calendar = new Google_Service_Calendar_Calendar();

		$calendar->setSummary( $data['summary'] );
		$calendar->setTimeZone( $data['timeZone'] );

		return $this->service->calendars->insert( $calendar );
	}

	public function createEvent( array $data )
	{
		if ( !is_array( $data ) || empty( $data ) || is_null( $this->calendar ) ) {
			return false;
		}

		$event = new Google_Service_Calendar_Event( $data );

		return $this->service->events->insert( $this->calendar, $event );
	}

	public function deleteEvent( $id )
	{
		return $this->service->events->delete( $this->calendar, $id );
	}

	public function getEvents($optParams = [])
	{
		$response = [];

		$list = $this->service->events->listEvents( $this->libConfig['room_email'], $optParams );

		$events = $list->getItems();

		while( true )
		{
			foreach ( $events as $event )
			{
				$response[] = $event;
			}

			$token = $list->getNextPageToken();

			if ( !$token ) {
				break;
			}
			
			$optParams['pageToken'] = $token;
			$events = $this->service->events->listEvents( $this->libConfig['room_email'], $optParams );
		}

		return $events;
	}

	public function __get( $var ) { return get_instance()->$var; }	
}