<?php

namespace CCHD\Media;

/**
 * Folder
 *
 * @package CCHD\Media
 */
class Folder
{
	// Property names
	const ID             = 'id';
	const NAME           = 'name';
	const TYPE           = 'type';
	const TAGS           = 'tags';
	const SHARED         = 'shared';

	// Folder types
	const TYPE_CAMPAIGNS = 'campaigns';
	const TYPE_CHANNELS  = 'channels';
	const TYPE_FLICKS    = 'flicks';
	const TYPE_MEDIA     = 'media';
	const TYPE_TEMPLATES = 'templates';

	// Shared values
	const SHARED_TRUE    = 'true';
	const SHARED_FALSE   = 'false';

	/** @var array */
	protected $properties = array(
		self::ID,
		self::NAME,
		self::TYPE,
		self::TAGS,
		self::SHARED
	);

	/**
	 * constructor
	 *
	 * @param array $properties
	 */
	public function __construct( array $properties )
	{
		// Set all properties to a default of NULL.
		$this->properties = array_fill_keys( $this->properties, null );

		// Assign the given properties.
		$this->properties = array_merge( $this->properties, array_intersect_key( $properties, $this->properties ) );
	}

	/**
	 * @param string $property
	 *
	 * @return mixed
	 */
	public function get( $property )
	{
		return @$this->properties[$property];
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->get( self::ID );
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->get( self::NAME );
	}

	/**
	 * @return string
	 */
	public function getTags()
	{
		return $this->get( self::TAGS );
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->get( self::TYPE );
	}

	/**
	 * @return bool
	 */
	public function isShared()
	{
		return ( $this->get( self::SHARED ) === self::SHARED_TRUE );
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return $this->properties;
	}

	/**
	 * @return string
	 */
	public static function generateId()
	{
		$datetime = \DateTime::createFromFormat( 'U.u', sprintf( '%.f', microtime( true ) ) );

		$now = array(
			'milliseconds' => round( $datetime->format( 'u' ) / 1000 ),
			'seconds'      => $datetime->format( 's' ),
			'minutes'      => $datetime->format( 'i' ),
			'hours'        => $datetime->format( 'G' ),
			'date'         => $datetime->format( 'j' ),
			'month'        => $datetime->format( 'n' ) - 1,
			'year'         => $datetime->format( 'y' )
		);

		$folder_id = number_format( floor(
			1111000000000000
			+ ( (
				$now['milliseconds']
				+ ( $now['seconds'] * 1000 )
				+ ( $now['minutes'] * 1000 * 60 )
				+ ( $now['hours']   * 1000 * 60 * 60 )
				+ ( $now['date']    * 1000 * 60 * 60 * 24 )
				+ ( $now['month']   * 1000 * 60 * 60 * 24 * 31 )
				+ ( $now['year']    * 1000 * 60 * 60 * 24 * 31 * 12 )
			) / 10 ) % 1000000000000
		), 0, '.', '' );

		return $folder_id;
	}
}
