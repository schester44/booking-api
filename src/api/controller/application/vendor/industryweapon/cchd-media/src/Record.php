<?php

namespace CCHD\Media;

/**
 * Record
 *
 * @package CCHD\Media
 */
class Record
{
	// Property names
	const ACTIVE                  = 'active';
	const APP_FLAG                = 'appFlag';
	const APPROVED                = 'approved';
	const ATTRIBUTES              = 'attribute';
	const BACKGROUND_IMAGE        = 'backgroundimage';
	const DATE                    = 'Date';
	const DESCRIPTION_NOTES       = 'descriptionNotes';
	const DEVICE_CACHE_INTERVAL   = 'deviceCacheInterval';
	const DURATION                = 'duration';
	const FLIPPIN                 = 'flippin';
	const FOLDER_ID               = 'folderID';
	const HIDDEN                  = 'hidden';
	const HIDE_MMHD               = 'hideMMHD';
	const ID                      = 'id';
	const MEDIAREF                = 'mediaref';
	const MEDIAREF_THUMB          = 'mediarefThumb';
	const MEDIAREF_PREVIEW        = 'mediarefPreview';
	const MODULE                  = 'module';
	const NAME                    = 'name';
	const OBJECT_ID               = 'objectid';
	const ORIENTATION             = 'orientation';
	const POWER_POINT             = 'powerPoint';
	const RELAY_LOCATION          = 'relayLocation';
	const SORT                    = 'sort';
	const TYPE                    = 'type';
	const URL                     = 'url';
	const USE_AS_LOGO             = 'useAsLogo';
	const USE_DEVICE_CACHE        = 'deviceCache';
	const USER                    = 'User';
	const VID_STATUS              = 'vidStatus';
	const VIDEO_QUALITY           = 'videoQuality';

	// Types
	const TYPE_FLASH              = 'Flash';
	const TYPE_HTML               = 'HTML';
	const TYPE_HTML_URL           = 'HTMLURL';
	const TYPE_IMAGE              = 'Image';
	const TYPE_VIDEO              = 'Video';
	const TYPE_VIDEO_URL          = 'videoURL';
	const TYPE_MIME_FLASH         = 'application/x-shockwave-flash';
	const TYPE_MIME_JPG           = 'image/jpeg';
	const TYPE_MIME_PNG           = 'image/png';
	const TYPE_MIME_TIFF          = 'image/tiff';

	// Flag values
	const FLAG_ON                 = 'on';
	const FLAG_OFF                = 'off';

	// Orientations
	const ORIENTATION_HORIZONTAL  = 'horizontal';
	const ORIENTATION_VERTICAL    = 'vertical';

	// Relay locations
	const RELAY_LOCATION_CCHD     = 'CCHD';
	const RELAY_LOCATION_CCRS     = 'CCRS';

	// Video statuses
	const VID_STATUS_READY        = 'Ready';

	// Video qualities
	const VIDEO_QUALITY_VERY_LOW  = 1;
	const VIDEO_QUALITY_LOW       = 2;
	const VIDEO_QUALITY_MEDIUM    = 3;
	const VIDEO_QUALITY_HIGH      = 4;
	const VIDEO_QUALITY_VERY_HIGH = 5;

	/** @var array */
	protected $properties = array(
		self::ACTIVE,
		self::APP_FLAG,
		self::APPROVED,
		self::ATTRIBUTES,
		self::BACKGROUND_IMAGE,
		self::DATE,
		self::DESCRIPTION_NOTES,
		self::DEVICE_CACHE_INTERVAL,
		self::DURATION,
		self::FLIPPIN,
		self::FOLDER_ID,
		self::HIDDEN,
		self::HIDE_MMHD,
		self::ID,
		self::MEDIAREF,
		self::MEDIAREF_THUMB,
		self::MEDIAREF_PREVIEW,
		self::MODULE,
		self::NAME,
		self::OBJECT_ID,
		self::ORIENTATION,
		self::POWER_POINT,
		self::RELAY_LOCATION,
		self::SORT,
		self::TYPE,
		self::URL,
		self::USE_AS_LOGO,
		self::USE_DEVICE_CACHE,
		self::USER,
		self::VID_STATUS,
		self::VIDEO_QUALITY
	);

	/**
	 * constructor
	 *
	 * @param array $properties
	 */
	public function __construct( array $properties )
	{
		// Set all properties to a default of NULL.
		$this->properties = array_fill_keys( $this->properties, null );

		// Assign the given properties.
		$this->properties = array_merge( $this->properties, array_intersect_key( $properties, $this->properties ) );
	}

	/**
	 * @param string $property
	 *
	 * @return mixed
	 */
	public function get( $property )
	{
		return @$this->properties[$property];
	}

	/**
	 * @param string $property
	 *
	 * @return bool
	 */
	public function is( $property )
	{
		return ( $this->get( $property ) === self::FLAG_ON );
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->get( self::ATTRIBUTES );
	}

	/**
	 * @param int|string $attribute
	 *
	 * @return bool
	 */
	public function hasAttribute( $attribute )
	{
		return in_array( $attribute, $this->getAttributes(), true );
	}

	/**
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->get( self::DATE );
	}

	/**
	 * @return string
	 */
	public function getDescriptionNotes()
	{
		return $this->get( self::DESCRIPTION_NOTES );
	}

	/**
	 * @return string
	 */
	public function getDeviceCacheInterval()
	{
		return $this->get( self::DEVICE_CACHE_INTERVAL );
	}

	/**
	 * @return string
	 */
	public function getDuration()
	{
		return $this->get( self::DURATION );
	}

	/**
	 * @return int
	 */
	public function getFolderId()
	{
		return $this->get( self::FOLDER_ID );
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->get( self::ID );
	}

	/**
	 * @return string
	 */
	public function getMediaref()
	{
		return $this->get( self::MEDIAREF );
	}

	/**
	 * @return string
	 */
	public function getMediarefThumb()
	{
		return $this->get( self::MEDIAREF_THUMB );
	}

	/**
	 * @return string
	 */
	public function getMediarefPreview()
	{
		return $this->get( self::MEDIAREF_PREVIEW );
	}

	/**
	 * @return string
	 */
	public function getModule()
	{
		return $this->get( self::MODULE );
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->get( self::NAME );
	}

	/**
	 * @return string
	 */
	public function getObjectId()
	{
		return $this->get( self::OBJECT_ID );
	}

	/**
	 * @return string
	 */
	public function getOrientation()
	{
		return $this->get( self::ORIENTATION );
	}

	/**
	 * @return string
	 */
	public function getRelayLocation()
	{
		return $this->get( self::RELAY_LOCATION );
	}

	/**
	 * @return string
	 */
	public function getSort()
	{
		return $this->get( self::SORT );
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->get( self::TYPE );
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->get( self::URL );
	}

	/**
	 * @return string
	 */
	public function getUser()
	{
		return $this->get( self::USER );
	}

	/**
	 * @return int
	 */
	public function getVideoQuality()
	{
		return $this->get( self::VIDEO_QUALITY );
	}

	/**
	 * @return string
	 */
	public function getVidStatus()
	{
		return $this->get( self::VID_STATUS );
	}

	/**
	 * @return bool
	 */
	public function hideMMHD()
	{
		return $this->is( self::HIDE_MMHD );
	}

	/**
	 * @return bool
	 */
	public function isActive()
	{
		return $this->is( self::ACTIVE );
	}

	/**
	 * @return bool
	 */
	public function isAppFlag()
	{
		return $this->is( self::APP_FLAG );
	}

	/**
	 * @return bool
	 */
	public function isApproved()
	{
		return $this->is( self::APPROVED );
	}

	/**
	 * @return bool
	 */
	public function isBackgroundImage()
	{
		return $this->is( self::BACKGROUND_IMAGE );
	}

	/**
	 * @return bool
	 */
	public function isFlippin()
	{
		return $this->is( self::FLIPPIN );
	}

	/**
	 * @return bool
	 */
	public function isHidden()
	{
		return $this->is( self::HIDDEN );
	}

	/**
	 * @return bool
	 */
	public function isPowerPoint()
	{
		return $this->is( self::POWER_POINT );
	}

	/**
	 * @return bool
	 */
	public function useAsLogo()
	{
		return $this->is( self::USE_AS_LOGO );
	}

	/**
	 * @return bool
	 */
	public function useDeviceCache()
	{
		return $this->is( self::USE_DEVICE_CACHE );
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return $this->properties;
	}

	/**
	 * @param string $file
	 *
	 * @return string|false
	 */
	public static function getTypeForFile( $file )
	{
		$extension_to_type = array(
			// images
			'jpg'  => Record::TYPE_IMAGE,
			'jpeg' => Record::TYPE_IMAGE,
			'jpe'  => Record::TYPE_IMAGE,
			'png'  => Record::TYPE_IMAGE,
			'tiff' => Record::TYPE_IMAGE,
			'bmp'  => Record::TYPE_IMAGE,

			// videos
			'mpg'  => Record::TYPE_VIDEO,
			'mpeg' => Record::TYPE_VIDEO,
			'mov'  => Record::TYPE_VIDEO,
			'flv'  => Record::TYPE_VIDEO,
			'mp4'  => Record::TYPE_VIDEO,
			'avi'  => Record::TYPE_VIDEO,
			'dv'   => Record::TYPE_VIDEO,
			'wmv'  => Record::TYPE_VIDEO,

			// misc
			'html' => Record::TYPE_HTML
		);

		$extension = strtolower( pathinfo( $file, PATHINFO_EXTENSION ) );

		return ( @$extension_to_type[$extension] ?: false );
	}
}
