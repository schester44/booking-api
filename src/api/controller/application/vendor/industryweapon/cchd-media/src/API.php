<?php

namespace CCHD\Media;

/**
 * API
 *
 * @package CCHD\Media
 */
class API
{
	// Operations
	const OP_ADD                      = 'add';
	const OP_ADD_REMOTE               = 'addRemote';
	const OP_DELETE                   = 'delete';
	const OP_DELETE_REMOTE            = 'deleteRemote';
	const OP_EDIT                     = 'edit';
	const OP_EDIT_REMOTE              = 'editRemote';
	const OP_LIST                     = 'list';
	const OP_LIST_FOLDERS             = 'folderList';
	const OP_LIST_REMOTE              = 'listRemote';
	const OP_VIEW                     = 'view';
	const OP_VIEW_REMOTE              = 'viewRemote';

	// Response statuses
	const STATUS_ERROR                = 'ERR';
	const STATUS_OK                   = 'OK';

	// Parameters
	const PARAM_ACTIVE                = 'active';
	const PARAM_APP_FLAG              = 'appFlag';
	const PARAM_APPROVED              = 'approved';
	const PARAM_ATTRIBUTES            = 'attribute';
	const PARAM_BACKGROUND_IMAGE      = 'backgroundimage';
	const PARAM_DESCRIPTION_NOTES     = 'descriptionNotes';
	const PARAM_DEVICE_CACHE          = 'deviceCache';
	const PARAM_DEVICE_CACHE_INTERVAL = 'deviceCacheInterval';
	const PARAM_FLIPPIN               = 'flippin';
	const PARAM_FOLDER_ID             = 'folderID';
	const PARAM_GROUP_OWNER           = 'groupowner';
	const PARAM_HIDDEN                = 'hidden';
	const PARAM_HORIZONTAL_OFFSET     = 'horizontalOffset';
	const PARAM_ID                    = 'id';
	const PARAM_LOGO_IMAGE            = 'logoImage';
	const PARAM_MEDIA                 = 'media';
	const PARAM_MEDIAREF              = 'mediaref';
	const PARAM_MODULE                = 'module';
	const PARAM_NAME                  = 'name';
	const PARAM_OBJECT_ID             = 'objectid';
	const PARAM_ORIENTATION           = 'orientation';
	const PARAM_OWNER                 = 'owner';
	const PARAM_POWER_POINT           = 'powerPoint';
	const PARAM_RELAY_LOCATION        = 'relayLocation';
	const PARAM_RESET_ACTIVE          = 'resetactive';
	const PARAM_RESET_APP_FLAG        = 'resetAppFlag';
	const PARAM_RESET_DEVICE_CACHE    = 'resetdevicecache';
	const PARAM_SET_ACTIVE            = 'setactive';
	const PARAM_SET_APP_FLAG          = 'setAppFlag';
	const PARAM_SET_DEVICE_CACHE      = 'setdeviceache';
	const PARAM_TYPE                  = 'type';
	const PARAM_URL                   = 'url';
	const PARAM_USE_AS_LOGO           = 'useAsLogo';
	const PARAM_USER                  = 'User';
	const PARAM_VERTICAL_OFFSET       = 'verticalOffset';
	const PARAM_VIDEO_QUALITY         = 'videoQuality';

	// Configuration options
	const CONFIG_TIMEOUT              = 'timeout';

	/** @var array */
	protected $valid_operations = array(
		self::OP_ADD,
		self::OP_ADD_REMOTE,
		self::OP_DELETE,
		self::OP_DELETE_REMOTE,
		self::OP_EDIT,
		self::OP_EDIT_REMOTE,
		self::OP_LIST,
		self::OP_LIST_FOLDERS,
		self::OP_LIST_REMOTE,
		self::OP_VIEW,
		self::OP_VIEW_REMOTE
	);

	/** @var array */
	protected $cacheable_operations = array(
		self::OP_LIST,
		self::OP_LIST_FOLDERS,
		self::OP_LIST_REMOTE
	);

	/** @var string */
	protected $account_id;

	/** @var string */
	protected $user_id;

	/** @var string */
	protected $password;

	/** @var array */
	protected $config = array(
		self::CONFIG_TIMEOUT => 10
	);

	/** @var resource */
	protected $curl;

	/**
	 * The response from the last API call.
	 *
	 * @var string
	 */
	protected $last_response;

	/**
	 * The unparsed response from the last API call.
	 *
	 * @var string
	 */
	protected $last_response_raw;

	/** @var array */
	protected $cache = array();

	/** @var array */
	private $falsy_values = array( Record::FLAG_OFF, false, null );

	/**
	 * constructor
	 *
	 * @param string $account_id
	 * @param string $user_id
	 * @param string $password
	 * @param array  $config
	 */
	public function __construct( $account_id, $user_id, $password, array $config = array() )
	{
		$this->account_id = $account_id;
		$this->user_id    = $user_id;
		$this->password   = $password;

		// Set any supplied options.
		foreach( array_keys( $this->config ) as $key ) {
			if( array_key_exists( $key, $config ) ) {
				$this->config[$key] = $config[$key];
			}
		}

		// Initialize a cURL resource.
		$this->curl = curl_init( "https://{$this->account_id}.channelshd.com/i7:CCHD.rpc_mediaAPI" );

		// Set some default cURL options.
		curl_setopt_array( $this->curl, array(
			CURLOPT_POST           => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_TIMEOUT        => $this->config[self::CONFIG_TIMEOUT]
		) );
	}

	/**
	 * Executes an API call.
	 *
	 * @param string $operation
	 * @param array  $parameters
	 *
	 * @return array
	 * @throws Exception\InvalidArgumentException
	 * @throws Exception\CurlException
	 * @throws Exception\HttpException
	 */
	public function exec( $operation, array $parameters = array() )
	{
		if( !in_array( $operation, $this->valid_operations ) ) {
			throw new Exception\InvalidArgumentException( "'{$operation}' is not a valid operation" );
		}

		$this->last_response     = null;
		$this->last_response_raw = null;

		// If we have a cached response for this operation, return it instead.
		if( array_key_exists( $operation, $this->cache ) ) {
			return $this->cache[$operation];
		}

		// Build and set our parameters.
		curl_setopt( $this->curl, CURLOPT_POSTFIELDS, $this->buildApiParameters( $operation, $parameters ) );

		// Execute the call.
		$curl_response = curl_exec( $this->curl );

		$curl_errno = curl_errno( $this->curl );
		$http_code  = curl_getinfo( $this->curl, CURLINFO_HTTP_CODE );

		if( $curl_errno !== 0 ) {
			throw new Exception\CurlException( curl_error( $this->curl ), $curl_errno );
		}

		if( $http_code !== 200 ) {
			throw new Exception\HttpException( $http_code );
		}

		$response = $this->parseResponse( $operation, $curl_response );

		$this->last_response     = $response;
		$this->last_response_raw = $curl_response;

		// Cache the results of this operation.
		if( in_array( $operation, $this->cacheable_operations ) ) {
			$this->cache[$operation] = $response;
		}

		return $response;
	}

	/**
	 * Clears the operation cache. If an operation is specified, only the cache for
	 * that operation is cleared.
	 *
	 * @param string $operation
	 *
	 * @return API
	 */
	public function clearCache( $operation = null )
	{
		if( $operation === null ) {
			$this->cache = array();
			return $this;
		}

		if( !in_array( $operation, $this->cacheable_operations, true ) ) {
			trigger_error( "'{$operation}' is not a cacheable operation; nothing to clear", E_USER_NOTICE );
			return $this;
		}

		if( array_key_exists( $operation, $this->cache ) ) {
			unset( $this->cache[$operation] );
		}

		return $this;
	}

	/**
	 * Returns an array containing all the necessary parameters to make an API
	 * call.
	 *
	 * @param string $operation
	 * @param array  $parameters
	 *
	 * @return array
	 */
	protected function buildApiParameters( $operation, array $parameters = array() )
	{
		$api_parameters['operation']    = $operation;
		$api_parameters['protoVersion'] = '1.0';
		$api_parameters['userid']       = $this->user_id;
		$api_parameters['timestamp']    = gmdate( 'Y/m/d H:i:s' );

		$api_parameters['signature'] = hash( 'sha512', implode( '|', array(
			'CCHDMEDIAAPI',
			$this->account_id,
			$this->user_id,
			$this->password,
			$api_parameters['timestamp']
		) ) );

		// If token authentication is being used, the password (which should be the
		// token) needs to be copied to the "token" parameter.
		if( $this->user_id === '$token' ) {
			$api_parameters['token'] = $this->password;
		}

		// Prevent overwriting of the API parameters we set above by removing any
		// occurrences from the given parameters.
		$parameters = array_diff_key( $parameters, $api_parameters );

		// Add the given parameters to the set of API parameters, doing any necessary
		// conversions.
		foreach( $parameters as $parameter => $value ) {
			switch( $parameter ) {

				// If these parameters are falsy, don't include them.
				case self::PARAM_APPROVED:
				case self::PARAM_BACKGROUND_IMAGE:
				case self::PARAM_FLIPPIN:
				case self::PARAM_HIDDEN:
				case self::PARAM_POWER_POINT:
				case self::PARAM_RESET_ACTIVE:
				case self::PARAM_RESET_APP_FLAG:
				case self::PARAM_RESET_DEVICE_CACHE:
				case self::PARAM_SET_ACTIVE:
				case self::PARAM_SET_APP_FLAG:
				case self::PARAM_SET_DEVICE_CACHE:
				case self::PARAM_USE_AS_LOGO:
					if( in_array( $value, $this->falsy_values, true ) ) {
						continue 2;
					}

					break;

				// The API does not accept the "active" flag for "edit" or "editRemote"
				// operations, so we need to convert it to either "setactive" or "resetactive",
				// depending on its value. If it's any other operation and the value is falsy,
				// don't include the parameter.
				//
				// TODO: Confirm that the "editRemote" part is correct.
				// The documentation implies that it should be "addRemote", but "editRemote" is
				// consistent with all of the other operations and flags.
				case self::PARAM_ACTIVE:
					if( $operation === self::OP_EDIT || $operation === self::OP_ADD_REMOTE ) {
						$parameter = ( in_array( $value, $this->falsy_values, true ) ? self::PARAM_RESET_ACTIVE : self::PARAM_SET_ACTIVE );
						$value     = true;
					} elseif( in_array( $value, $this->falsy_values, true ) ) {
						continue 2;
					}

					break;

				// If this is an "edit" operation, we need to convert the "appFlag" parameter to
				// either "setAppFlag" or "resetAppFlag", depending on its value. If it's any
				// other operation, and its value is falsy, don't include the parameter.
				case self::PARAM_APP_FLAG:
					if( $operation === self::OP_EDIT || $operation === self::OP_EDIT_REMOTE ) {
						$parameter = ( in_array( $value, $this->falsy_values, true ) ? self::PARAM_RESET_APP_FLAG : self::PARAM_SET_APP_FLAG );
						$value     = true;
					} elseif( in_array( $value, $this->falsy_values, true ) ) {
						continue 2;
					}

					break;

				// The API does not actually accept the "deviceCache" property, so we need to
				// convert it to either "setdevicecache" or "resetdevicecache", depending on its
				// value.
				case self::PARAM_DEVICE_CACHE:
					$parameter = ( in_array( $value, $this->falsy_values, true ) ? self::PARAM_RESET_DEVICE_CACHE : self::PARAM_SET_DEVICE_CACHE );
					$value     = true;

					break;

				// If the "attribute" parameter is an array, convert it to a comma-separated
				// string.
				case self::PARAM_ATTRIBUTES:
					if( is_array( $value ) ) {
						$value = implode( ',', $value );
					}

					break;
			}

			$api_parameters[$parameter] = $value;
		}

		return $api_parameters;
	}

	/**
	 * @param string $operation
	 * @param string $response
	 *
	 * @return array
	 */
	protected function parseResponse( $operation, $response )
	{
		$response_parts = explode( "\n", $response, 2 );

		$status = $response_parts[0];
		if( $status !== self::STATUS_OK ) {
			$status_parts = explode( ': ', $status, 2 );
			return array(
				'status'  => $status_parts[0],
				'message' => $status_parts[1]
			);
		}

		$body = rtrim( $response_parts[1], "\n" );

		$data = null;
		switch( $operation ) {
			case self::OP_ADD:
			case self::OP_EDIT:
				$body_parts = explode( ':', $body );
				$data = array(
					Record::OBJECT_ID        => $body_parts[0],
					Record::MEDIAREF         => $body_parts[1],
					Record::MEDIAREF_THUMB   => $body_parts[2],
					Record::MEDIAREF_PREVIEW => $body_parts[3]
				);
				break;

			case self::OP_ADD_REMOTE:
			case self::OP_EDIT_REMOTE:
				$body_parts = explode( ':', $body );
				$data = array(
					Record::OBJECT_ID => $body_parts[0],
					Record::ID        => $body_parts[1]
				);
				break;

			case self::OP_LIST:
				$data = $this->parseRecordList( $body );
				break;

			case self::OP_LIST_FOLDERS:
				$data = $this->parseFolderList( $body );
				break;

			case self::OP_LIST_REMOTE:
				$data = $this->parseRemoteRecordList( $body );
				break;

			case self::OP_VIEW:
				$data = $this->parseRecordList( $body );
				$data = $data[0];
				break;

			case self::OP_VIEW_REMOTE:
				$data = $this->parseRemoteRecordList( $body );
				$data = $data[0];
				break;
		}

		return array(
			'status' => $status,
			'data'   => $data
		);
	}

	/**
	 * @param string $body
	 *
	 * @return Record[]
	 */
	protected function parseRecordList( $body )
	{
		$properties = array(
			Record::OBJECT_ID,
			Record::ID,
			Record::NAME,
			Record::TYPE,
			Record::ACTIVE,
			Record::DATE,
			Record::MEDIAREF,
			Record::MEDIAREF_THUMB,
			Record::MEDIAREF_PREVIEW,
			Record::VID_STATUS,
			Record::BACKGROUND_IMAGE,
			Record::ORIENTATION,
			Record::FLIPPIN,
			Record::VIDEO_QUALITY,
			Record::FOLDER_ID,
			Record::HIDDEN,
			Record::POWER_POINT,
			Record::USE_AS_LOGO,
			Record::APPROVED,
			Record::USER,
			Record::ATTRIBUTES,
			Record::DESCRIPTION_NOTES,
			Record::DURATION,
			Record::SORT,
			Record::HIDE_MMHD,
			'_unknown_flag_1',
			'_unknown_flag_2',
			Record::APP_FLAG
		);

		$records = array();
		foreach( $this->parseListBody( $body, $properties ) as $record ) {
			foreach( $record as $property => &$value ) {
				switch( $property ) {

					// Convert the date field to a DateTime object. Since the returned string has no
					// timezone information, but is known to be eastern U.S., we need to explicitly
					// set the timezone.
					case Record::DATE:
						$value = new \DateTime( $value, new \DateTimeZone( 'America/New_York' ) );
						break;

					// Convert the attribute field to an array.
					case Record::ATTRIBUTES:
						$value = explode( ',', $value );
						break;
				}
			}

			$records[] = new Record( $record );
		}

		return $records;
	}

	/**
	 * @param string $body
	 *
	 * @return Folder[]
	 */
	protected function parseFolderList( $body )
	{
		$properties = array(
			Folder::ID,
			Folder::NAME,
			Folder::TYPE,
			Folder::TAGS,
			Folder::SHARED
		);

		$folders = array();
		foreach( $this->parseListBody( $body, $properties ) as $folder ) {
			$folders[] = new Folder( $folder );
		}

		return $folders;
	}

	/**
	 * @param string $body
	 *
	 * @return Record[]
	 */
	protected function parseRemoteRecordList( $body )
	{
		$properties = array(
			Record::OBJECT_ID,
			Record::ID,
			Record::NAME,
			Record::TYPE,
			Record::URL,
			Record::ACTIVE,
			Record::USE_DEVICE_CACHE,
			Record::DEVICE_CACHE_INTERVAL,
			Record::RELAY_LOCATION,
			Record::MODULE,
			Record::FOLDER_ID
		);

		$records = array();
		foreach( $this->parseListBody( $body, $properties ) as $record ) {
			$records[] = new Record( $record );
		}

		return $records;
	}

	/**
	 * @param string $body
	 * @param array  $field_names
	 *
	 * @return array
	 */
	protected function parseListBody( $body, array $field_names )
	{
		$rows = array();
		foreach( explode( "\n", $body ) as $i => $row ) {
			$fields = explode( "\t", $row );

			$row = array();
			foreach( $field_names as $j => $field_name ) {

				// Skip missing or empty fields.
				if( !isset( $fields[$j] ) || $fields[$j] === '' ) {
					continue;
				}

				$row[$field_name] = $fields[$j];
			}

			$rows[] = $row;
		}

		return $rows;
	}

	/**
	 * @return Record[]|false
	 */
	public function getRecords()
	{
		$response = $this->exec( self::OP_LIST );

		return ( $response['status'] === self::STATUS_OK ) ? $response['data'] : false;
	}

	/**
	 * @param string $object_id
	 *
	 * @return Record|false
	 */
	public function getRecord( $object_id )
	{
		$response = $this->exec( self::OP_VIEW, array(
			self::PARAM_OBJECT_ID => $object_id
		) );

		return ( $response['status'] === self::STATUS_OK ) ? $response['data'] : false;
	}

	/**
	 * @param string $name
	 *
	 * @return Record|null|false
	 */
	public function getRecordByName( $name )
	{
		$records = $this->getRecords();
		if( $records === false ) {
			return false;
		}

		foreach( $records as $record ) {
			if( $record->getName() === $name ) {
				return $record;
			}
		}

		return null;
	}

	/**
	 * @return Folder[]|false
	 */
	public function getFolders()
	{
		$response = $this->exec( self::OP_LIST_FOLDERS );

		return ( $response['status'] === self::STATUS_OK ) ? $response['data'] : false;
	}

	/**
	 * @param int $id
	 *
	 * @return Folder|null|false
	 */
	public function getFolder( $id )
	{
		$folders = $this->getFolders();
		if( $folders === false ) {
			return false;
		}

		foreach( $folders as $folder ) {
			if( $folder->getId() === $id ) {
				return $folder;
			}
		}

		return null;
	}

	/**
	 * @param string $name
	 *
	 * @return Folder|null|false
	 */
	public function getFolderByName( $name )
	{
		$folders = $this->getFolders();
		if( $folders === false ) {
			return false;
		}

		foreach( $folders as $folder ) {
			if( $folder->getName() === $name ) {
				return $folder;
			}
		}

		return null;
	}

	/**
	 * @param int $folder_id
	 *
	 * @return Record[]|false
	 */
	public function getFolderContents( $folder_id )
	{
		$records = $this->getRecords();
		if( $records === false ) {
			return false;
		}

		$folder_contents = array();
		foreach( $records as $record ) {
			if( $record->getFolderId() === $folder_id ) {
				$folder_contents[] = $record;
			}
		}

		return $folder_contents;
	}

	/**
	 * @param string $filename
	 * @param string $name
	 * @param string $type
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function add( $filename, $name, $type, array $parameters = array() )
	{
		$add_parameters = array(
			self::PARAM_ACTIVE      => true,
			self::PARAM_GROUP_OWNER => 'DefaultContent',
			self::PARAM_MEDIA       => "@{$filename}",
			self::PARAM_NAME        => $name,
			self::PARAM_OWNER       => $this->user_id,
			self::PARAM_TYPE        => $type
		);

		// Take any parameters we haven't explicitly set above and add them to the
		// parameters array.
		$add_parameters = array_merge( $add_parameters, array_diff_key( $parameters, $add_parameters ) );

		$response = $this->exec( self::OP_ADD, $add_parameters );

		return ( $response['status'] === self::STATUS_OK ) ? $response['data'] : false;
	}

	/**
	 * @param string $filename
	 * @param string $name
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function addHtml( $filename, $name, array $parameters = array() )
	{
		return $this->add( $filename, $name, Record::TYPE_HTML, $parameters );
	}

	/**
	 * @param string $filename
	 * @param string $name
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function addImage( $filename, $name, array $parameters = array() )
	{
		return $this->add( $filename, $name, Record::TYPE_IMAGE, $parameters );
	}

	/**
	 * @param string $filename
	 * @param string $name
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function addVideo( $filename, $name, array $parameters = array() )
	{
		return $this->add( $filename, $name, Record::TYPE_VIDEO, $parameters );
	}

	/**
	 * @param string $filename
	 * @param string $name
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function addFlash( $filename, $name, array $parameters = array() )
	{
		return $this->add( $filename, $name, Record::TYPE_FLASH, $parameters );
	}

	/**
	 * @param string $url
	 * @param string $name
	 * @param string $type
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function addRemote( $url, $name, $type, array $parameters = array() )
	{
		$add_parameters = array(
			self::PARAM_ACTIVE      => true,
			self::PARAM_GROUP_OWNER => 'DefaultContent',
			self::PARAM_NAME        => $name,
			self::PARAM_OWNER       => $this->user_id,
			self::PARAM_TYPE        => $type,
			self::PARAM_URL         => $url
		);

		// Take any parameters we haven't explicitly set above and add them to the
		// parameters array.
		$add_parameters = array_merge( $add_parameters, array_diff_key( $parameters, $add_parameters ) );

		$response = $this->exec( self::OP_ADD_REMOTE, $add_parameters );

		return ( $response['status'] === self::STATUS_OK ) ? $response['data'] : false;
	}

	/**
	 * @param string $url
	 * @param string $name
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function addRemoteImage( $url, $name, array $parameters = array() )
	{
		return $this->addRemote( $url, $name, Record::TYPE_IMAGE, $parameters );
	}

	/**
	 * @param string $url
	 * @param string $name
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function addRemoteHtml( $url, $name, array $parameters = array() )
	{
		return $this->addRemote( $url, $name, Record::TYPE_HTML, $parameters );
	}

	/**
	 * @param string $object_id
	 * @param string $filename
	 * @param string $type
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function edit( $object_id, $filename, $type, array $parameters = array() )
	{
		$edit_parameters = array(
			self::PARAM_MEDIA     => "@{$filename}",
			self::PARAM_OBJECT_ID => $object_id,
			self::PARAM_TYPE      => $type
		);

		// Take any parameters we haven't explicitly set above and add them to the
		// parameters array.
		$edit_parameters = array_merge( $edit_parameters, array_diff_key( $parameters, $edit_parameters ) );

		$response = $this->exec( self::OP_EDIT, $edit_parameters );

		return ( $response['status'] === self::STATUS_OK ) ? $response['data'] : false;
	}

	/**
	 * @param string $object_id
	 * @param string $filename
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function editHtml( $object_id, $filename, array $parameters = array() )
	{
		return $this->edit( $object_id, $filename, Record::TYPE_HTML, $parameters );
	}

	/**
	 * @param string $object_id
	 * @param string $filename
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function editImage( $object_id, $filename, array $parameters = array() )
	{
		return $this->edit( $object_id, $filename, Record::TYPE_IMAGE, $parameters );
	}

	/**
	 * @param string $object_id
	 * @param string $filename
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function editVideo( $object_id, $filename, array $parameters = array() )
	{
		return $this->edit( $object_id, $filename, Record::TYPE_VIDEO, $parameters );
	}

	/**
	 * @param string $object_id
	 * @param string $filename
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function editFlash( $object_id, $filename, array $parameters = array() )
	{
		return $this->edit( $object_id, $filename, Record::TYPE_FLASH, $parameters );
	}

	/**
	 * @param string $object_id
	 * @param string $url
	 * @param string $type
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function editRemote( $object_id, $url, $type, array $parameters = array() )
	{
		$edit_parameters = array(
			self::PARAM_OBJECT_ID => $object_id,
			self::PARAM_TYPE      => $type,
			self::PARAM_URL       => $url
		);

		// Take any parameters we haven't explicitly set above and add them to the
		// parameters array.
		$edit_parameters = array_merge( $edit_parameters, array_diff_key( $parameters, $edit_parameters ) );

		$response = $this->exec( self::OP_EDIT_REMOTE, $edit_parameters );

		return ( $response['status'] === self::STATUS_OK ) ? $response['data'] : false;
	}

	/**
	 * @param string $object_id
	 * @param string $url
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function editRemoteImage( $object_id, $url, array $parameters = array() )
	{
		return $this->editRemote( $object_id, $url, Record::TYPE_IMAGE, $parameters );
	}

	/**
	 * @param string $object_id
	 * @param string $url
	 * @param array  $parameters
	 *
	 * @return array|false
	 */
	public function editRemoteHtml( $object_id, $url, array $parameters = array() )
	{
		return $this->editRemote( $object_id, $url, Record::TYPE_HTML, $parameters );
	}

	/**
	 * @param string $object_id
	 *
	 * @return bool
	 */
	public function delete( $object_id )
	{
		$response = $this->exec( self::OP_DELETE, array(
			self::PARAM_OBJECT_ID => $object_id
		) );

		return ( $response['status'] === self::STATUS_OK );
	}

	/**
	 * @param string $object_id
	 *
	 * @return bool
	 */
	public function deleteRemote( $object_id )
	{
		$response = $this->exec( self::OP_DELETE_REMOTE, array(
			self::PARAM_OBJECT_ID => $object_id
		) );

		return ( $response['status'] === self::STATUS_OK );
	}

	/**
	 * Returns the response from the last API call. If 'raw' is TRUE, returns
	 * the unparsed response string.
	 *
	 * @param bool $raw
	 *
	 * @return array|string
	 */
	public function getLastResponse( $raw = false )
	{
		return ( $raw ? $this->last_response_raw : $this->last_response );
	}

	/**
	 * destructor
	 */
	public function __destruct()
	{
		curl_close( $this->curl );
	}
}
