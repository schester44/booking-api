<?php

namespace CCHD\Media\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}
