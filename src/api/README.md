# App Framework - Backend

This is a "starter" repository that can be used as the basis of an app.

Note: Uses of `yarn` in the steps below can be replaced with `npm` if
you do not have [Yarn](https://yarnpkg.com/en/) installed.

## Initial Setup

1.  `git clone --recursive <repo>`
2.  `cd <directory>`
3.  `yarn install`

## Updating

1.  `yarn run update`

The first thing this does is pull updates from the repo. This is
primarily useful for updating a "remote" clone of the repo before
building and packaging the app.

The second thing this does is update any submodules.

## Cleaning

This will delete both the build/ and dist/ directories.

1.  `yarn run clean`

## Building

You need to follow the steps under "Cleaning", above, before following
the steps below.

1.  `yarn run build`

### Building for Internal Testing

If you're building and packaging this app for internal testing, there
are a couple things that need to be considered:

*   If the app already exists within the app system, installing a newer
    version (even for internal testing) will make that version available
    as an upgrade. That's bad. The workaround is to make the appcode for
    the app being tested different from the appcode for the released
    app.
*   If the app being tested is not set to private, once installed, it
    will become available to all customers. That's also bad. The
    workaround is to make the app being tested private, even if it's
    intended to be a public app.

Both of those workarounds involve editing package.json, which kind of
pollutes the package and requires remembering to undo those changes
before creating the release package. That's not ideal.

Fortunately, the packaging script will take care of these automatically,
building the app package with an appcode of "<appcode>-testing" and
creating the "privateApp" meta file.

### Building for Production

1.  `yarn run build -- --production`

This will build the app package with the appcode you specified in
package.json and not create the "privateApp" meta file (if the
config.private flag is also not set in package.json).

## Packaging

1.  `yarn run package`

You can find the final compressed archive in the dist/ directory.
