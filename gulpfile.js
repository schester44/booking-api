const path = require("path")
const del = require("del")
const file = require("gulp-file")
const fs = require("fs")
const gulp = require("gulp")
const gulpif = require("gulp-if")
const gutil = require("gulp-util")
const gzip = require("gulp-gzip")
const mkdirp = require("mkdirp")
const pump = require("pump")
const rename = require("gulp-rename")
const tar = require("gulp-tar")
const debug = require("gulp-debug")

const prettylog = require('@iw/pretty-log')

const argv = require("yargs").argv
const pkg = require("./package.json")

prettylog(`${pkg.name} - v${pkg.version}`)

const paths = {
	src: {
		controller: {
			base: [
				"src/api/controller/index.php",
				"src/api/controller/{application,system}/**",
				"!src/api/controller/application/cache{,/**}",
				"!src/api/controller/application/config/{development,testing}{,/**}",
				"src/api/controller/application/data/themes",
				"src/api/controller/application/data/gcal-credentials.json",
				"!src/api/controller/application/data/{images,queue}{,/**}",
				"!src/api/controller/application/templates"
			],
			dashboard: ["src/dashboard/dist/**"],
			app: ["src/app/build/**"]
		}
	},
	dest: {
		build: "build",
		dist: "dist"
	}
}

//==============================================================================
// Tasks
//==============================================================================

/**
 * Tarballs the contents of the build directory and saves it to the dist
 * directory as "<appcode>-<version>.tar.gz".
 */
gulp.task("package", done => {
	const appcode = fs.readFileSync(path.join(paths.dest.build, "appcode"))
	const file = `${appcode}-${pkg.version}.tar`

	prettylog(file, { color: 'magenta', heading: 'PACKAGING' })

	pump(
		[gulp.src(path.join(paths.dest.build, "**"), { dot: true }), tar(file), gzip(), gulp.dest(paths.dest.dist)],
		done
	)
})

/**
 * Creates meta files and copies files and directories from the backend and
 * dashboard, organizing them in a way that conforms to the app package
 * structure.
 */
gulp.task("build", ["build:controller", "build:daemon", "build:share"], done => {
	prettylog('Building...')

	let appcode = pkg.config.appcode

	if (argv.production !== true) {
		appcode += "-testing"
	}

	const isPrivate = pkg.config.private === true || argv.production !== true

	prettylog( appcode, { color: "magenta", heading: 'APPCODE' })
	prettylog( isPrivate, { color: "magenta", heading: 'PRIVATE?' })

	pump(
		[
			gulp.src(pkg.config.icon),
			rename("icon"),
			file("appcode", appcode),
			file("controllerTasks", pkg.config.tasks.controller.join("\n")),
			file("daemonTasks", ""),
			file("description", pkg.description),
			file("devcode", pkg.config.devcode),
			file("devID", pkg.config.devID),
			file("minAA", pkg.config.minAA),
			file("minCCHD", pkg.config.minCCHD),
			gulpif(isPrivate, file("privateApp", "")),
			file("version", pkg.version),
			gulp.dest(paths.dest.build)
		],
		done
	)
})

/**
 * Creates the "controllerPack" directory and copies appropriate files and
 * directories from the backend and dashboard into that directory.
 */
gulp.task("build:controller", ["build:controller:base", "build:controller:dashboard", "build:controller:app"], done => {
	pump(
		[
			gulp.src("src/api/controller/index.php"),
			rename("control.php"),
			gulp.dest(path.join(paths.dest.build, "controllerPack")),
			rename("ui.php"),
			gulp.dest(path.join(paths.dest.build, "controllerPack"))
		],
		done
	)
})

/**
 * Copies files that make up the "base" of the controller.
 */
gulp.task("build:controller:base", done => {
	pump(
		[gulp.src(paths.src.controller.base, { dot: true }), gulp.dest(path.join(paths.dest.build, "controllerPack"))],
		done
	)
})

/**
 * Copies files and directories for the dashboard part of the controller.
 */
gulp.task("build:controller:dashboard", done => {
	pump(
		[
			gulp.src(paths.src.controller.dashboard, { dot: true }),
			gulp.dest(path.join(paths.dest.build, "controllerPack", "dashboard"))
		],
		done
	)
})

/**
 * Copies files and directories for the app part of the controller.
 */
gulp.task("build:controller:app", done => {
	pump(
		[
			gulp.src(paths.src.controller.app, { dot: true }),
			gulp.dest(path.join(paths.dest.build, "controllerPack", "app"))
		],
		done
	)
})

/**
 * Creates the "daemonPack" directory and the required control.php file.
 */
gulp.task("build:daemon", done => {
	pump([file("control.php", "$OK", { src: true }), gulp.dest(path.join(paths.dest.build, "daemonPack"))], done)
})

/**
 * Creates the "sharePack" directory.
 */
gulp.task("build:share", done => {
	mkdirp(path.join(paths.dest.build, "sharePack"), done)
})

/**
 * Deletes both the build and dist directories.
 */
gulp.task("clean", done => {
	del(Object.keys(paths.dest).map(key => paths.dest[key])).then(paths => {
		paths.forEach(path => prettylog(path, { color: "magenta", heading: "deleted", headingColor: "green"}))
		done()
	})
})
