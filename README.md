# Conference Room Booking App


## Getting Started

Kiosk - https://bitbucket.org/schester44/react-booking-app

Dashboard - https://bitbucket.org/schester44/booking-dashboard

### Installation
1. git clone --recursive git@bitbucket.org:schester44/booking-api.git
2. cd bookin-api
3. npm install

### Development
1. rename `src/dashboard/.npmrc-example` to `src/dashboard/.npmrc` and set any variables if needed
2. Set your base_url in `src/api/controller/application/config/config.php`
3. `cd ./src/dashboard && npm start`
4. `cd ./src/app && npm start`

### Deployment
1. `npm run deploy--testing` (for testing)
2. `npm run deploy` (for production)

## Available Commands

**npm install** - will install all dependencies for all 3 parts of the app. (dashboard, app, api)

**npm run build** 

- Creates necessary files for an app, to package - run npm run package

**npm run deploy** 

- Creates an app tar file for production

**npm run deploy-testing** 

- Creates an app tar file for testing

**npm run clean** 

- Deletes build and dist folders

**npm run package** 

- Creates a package of build files (must be called after build)

**npm run update** 

- Does a git pull and updates the submodules 